<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'Welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['auth'] = "api/Key/login_auth";
$route['rokuActivation'] = "api/Activation/box_activation";
$route['registration'] = "api/user/users";
$route['suspend'] = "api/user/suspend";
$route['resetPassword'] = "api/user/resetPassword";
$route['lycaDevice'] = "api/LycaStb/lyca_activation";
$route['accountCreationReport'] = "api/user/report";
$route['getUserDetails'] = "api/user/getUser";
$route['additionalDevice'] = "api/AdditionalDevice/additional_device";
$route['tentkotta'] = "api/Tentkotta/tentkotta";
$route['erosnow'] = "api/Erosnow/erosnow";
$route['upgrade'] = "api/Upgrade/upgrade";
$route['renew'] = "api/Renew/renew";
$route['reactivate'] = "api/Reactivate/reactivate";
$route['cancel'] = "api/Cancel/cancel";
$route['suspend'] = "api/Suspend/suspend";

$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
