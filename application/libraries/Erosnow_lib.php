﻿<?php

Class erosnow_lib {



    private $ck; //Consumer key.
    private $cs; //Consumer secret.
    private $tk; //token.
    private $ts; //token secret.
    private $consumer_key = 'fa990c2c3090eb35b78ed3d6ab140c67057760c3e';
    private $consumer_secret = 'a43b0f2859844c587624cc198a9966ba';

    function __construct() {

        $this->ck = $this->consumer_key;
        $this->cs = $this->consumer_secret;
    }

    function setTokenAndSecret($token, $token_secret) {
        $this->tk = $token;
        $this->ts = $token_secret;

    }

    private function createSignature($method, $url, &$params) {
        $version = "1.0";
        if (!is_array($params))
            $params = array();
        $params['oauth_signature_method'] = "HMAC-SHA1";
        $params['oauth_timestamp'] = time();
        $params['oauth_version'] = "1.0";
        $params['oauth_consumer_key'] = $this->ck;
        $params['oauth_nonce'] = md5(uniqid(rand(), true));
        $params['oauth_token'] = $this->tk;
        ksort($params);
        $sp = "";
        foreach ($params as $k => $v) {
            if (!empty($sp)) {
                $sp = $sp . "&";
            }
            $sp = $sp . $k . "=" . rawurlencode($v);
        }
        $basestring = strtoupper($method) . "&" . rawurlencode($url) . "&" . rawurlencode($sp);
         //echo 'base_string <br>';
        // print $basestring;
         //echo '<br>';
        $hmackey = $this->cs . "&" . $this->ts;
        return base64_encode(hash_hmac("sha1", $basestring, $hmackey, true));
    }

    public function sendData($url, $params, $method, $json = true) {

        $p = $params;
        $p['oauth_signature'] = $this->createSignature($method, $url, $p);
        $a = "";
        foreach ($p as $k => $v) {
            if (!empty($a)) {
                $a = $a . ",";
            }
            $a = $a . $k . "=" . rawurlencode($v);
        }

        $header[] = "Authorization: OAuth " . $a;
      		
         // $header[] = "";
          
        // echo "here";exit();

        return $this->curl($url, $params, $method, $header, $json);
    }

    /**
     * @methodType: Possible values are "GET", "POST", "PUT", & "DELETE"
     * @output: Possible values are "TEXT": "JSON_OBJECT", & "JSON_ARRAY"
     * @data: Data array of key-value tuples. Example: array('name' => 'myname')
     */
    private function curl($url, $data = NULL, $method = 'GET', $header = NULL, $json = true, $timeout = 60) {
        $s = curl_init(); // initialize curl handler

        curl_setopt($s, CURLOPT_TIMEOUT, $timeout); //time out of the curl handler
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, $timeout); //time out of the curl socket connection closing
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE); // set option curl to return as string ,don't output directly
        curl_setopt($s, CURLOPT_FOLLOWLOCATION, 1);
      //  curl_setopt($s,CURLOPT_SSL_VERIFYPEER, false);


        if ($header)
            curl_setopt($s, CURLOPT_HTTPHEADER, $header); //set headers if presents   
       //  echo 'header <br>';        
       //  var_dump($header);
        // echo '<br>';
        $dataString = "";
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $dataString .= $key . '=' . urlencode($value) . '&';
            }
        }
        rtrim($dataString, '&');

        if (strtolower($method) == 'get' && !empty($dataString)) {
            $url = $url . "?" . $dataString;
        } else if (strtolower($method) == 'post') {
            curl_setopt($s, CURLOPT_POST, TRUE); //set curl option to post method
            curl_setopt($s, CURLOPT_POSTFIELDS, $dataString);
        } else if (strtolower($method) == 'delete') {
            curl_setopt($s, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($s, CURLOPT_POSTFIELDS, $dataString);
        } else if (strtolower($method) == 'put') {
            curl_setopt($s, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($s, CURLOPT_POSTFIELDS, $dataString);
        }
       // echo 'url <br>';
       // print $url."\n";
      //  echo '<br>';
        curl_setopt($s, CURLOPT_URL, $url); //set option  URL of the location

        $data = curl_exec($s);
        $code = curl_getinfo($s, CURLINFO_HTTP_CODE); // get the response status

        curl_close($s); //close handler
        // echo 'code <br>';
        // print_r($code);
       //  echo '<br>';
//exit();
        if ($json) {
            return json_decode($data, TRUE);
        }
        return $data;
    }

// User Specific Erosnow OAuth Token Secret
}