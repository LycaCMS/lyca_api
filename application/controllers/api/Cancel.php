<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Cancel extends REST_Controller {

    protected $methods = [
          'box_activation_post' => ['level' => 10],
        ];




function cancel_post() {
    
        $user_id = $this->post("userId");        
        
        
         if (!$user_id=="" && !(preg_match('/[^a-z_\-0-9]/i', $user_id))) {
           
        if ($this->user_model->check_data("payment_circle", array('payment_circle_id' =>$user_id)) < 1) {
                
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'This Customer Do not have Registered Account',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
                }                
         
                        
   
             else {               

                                  
                    $cus_id=$this->user_model->get_data('payment_circle','*', array('payment_circle_id' =>$user_id))->row()->cus_id;
                 
                    $check_cus_status=$this->user_model->get_data('customers','*', array('cus_id' => $cus_id))->row()->customer_status;
                   
                   
                    $cus_end_date = $this->user_model->get_customer($cus_id, "end_date");
                    $today = $this->user_model->todaydate();              


                     if($cus_end_date<$today){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Customer Subscription has Expired'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                    
                    if($check_cus_status==2 || $check_cus_status==5){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Customer Account has Expired'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                        if($check_cus_status==4 || $check_cus_status==6){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Account is already Cancelled'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                       //  $old_st = $this->user_model->get_status_type($stat);
                       //  $new_st = $this->user_model->get_status_type($status);

                        $today_date = $this->user_model->todaydate();

                    
                        $this->db->where("cus_id", $cus_id);
                        $stb = $this->db->get("stb");
                        
                        foreach ($stb->result_array() as $rr) {
                            //$this->gm->sesspended_xml($rr['stb_serial_name']);
                        }

                         $data_cus = array(
                           'customer_status' => 4
                        );

                         $check_data = $this->user_model->update_data('customers', $data_cus, array('cus_id' => $cus_id));



                         $data = array(
                            'suspended_date' => date('Y-m-d')
                          );


                        $this->db->where("cus_id", $cus_id);


                        $this->db->update("customers", $data);

                        $tuk = $this->uk_convert($today_date);
                        $this->user_model->customers_log($cus_id, 11, $this->user_model->timenow(), " was updated customer status into Cancelled by $tuk", 0);
                        $this->user_model->tentkotta_api_log($cus_id, 11, $this->user_model->timenow(), " was updated customer status into Cancelled by $tuk", 0);

                            $this->response([
                                'status' => REST_Controller::HTTP_CREATED,
                                'message' => 'Customer Account has Cancelled',
                                    ], REST_Controller::HTTP_CREATED);
                        }
                     
                  }

        else {       
                   $this->response([
                           'status' => REST_Controller::HTTP_BAD_REQUEST,
                           'message' => 'Invalid parameters',
                              ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
           }

       }
       
       function uk_convert($date, $format = "%d-%m-%Y") {
            return mdate($format, strtotime($date));
        }
    
 }



