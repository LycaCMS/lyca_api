<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Upgrade extends REST_Controller {

    protected $methods = [
          'box_activation_post' => ['level' => 10],
        ];




function upgrade_post() {
    

        $user_id = $this->post("userId");        
        $new_upgrade_subscription = $this->post("subscription"); 

        
         if (!$user_id==""  &&  $new_upgrade_subscription!="" && !(preg_match('/[^a-z_\-0-9]/i', $user_id))) {
           
        if ($this->user_model->check_data("payment_circle", array('payment_circle_id' =>$user_id)) < 1) {
                
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'This Customer Do not have Registered Account',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
                }                
         
   
             else {               

                                  
                    $cus_id=$this->user_model->get_data('payment_circle','*', array('payment_circle_id' =>$user_id))->row()->cus_id;
                 
                    $check_cus_status=$this->user_model->get_data('customers','*', array('cus_id' => $cus_id))->row()->customer_status;
                   
                   
                    if(($check_cus_status!=1)){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Customer is not allowed for Upgrade the Account'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                    $subscriptions_arr=array(7,8,9,10);

                     if(!in_array($new_upgrade_subscription,$subscriptions_arr)){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Invalid Subscription'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }
                    

                     $old_subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
                   
                   $old_package_days=$this->user_model->get_data('subscription_periods','*', array('subscription_period_id' => $old_subscription_period_id))->row()->no_of_days;
                   $new_package_days=$this->user_model->get_data('subscription_periods','*', array('subscription_period_id' => $new_upgrade_subscription))->row()->no_of_days;
                   
                   if($new_package_days<=$old_package_days){
                       $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'New Subscription should be higher than the current plan'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                   }
        
        $postArray['payment_method']=30;
            
       // $is_paied['err_type']=1;
        
        
      
        //$cus_id = $this->input->post("cus_id");
        //$postArray = $this->input->post();
        
        $bank_code = "";
        if ($this->input->post("bank_ref_code") != '') {
            $bank_code = $this->input->post("bank_ref_code");
        }
        $pending_stb_cnt = 0;

        $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
        $cus_email = $this->user_model->get_customer($cus_id, "email");
        $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as m_id", "m_id");


        $customer_status = $this->user_model->get_customer($cus_id, "customer_status");


        $payment_type = 2;
        $pymntDtlsArray = array();
        
        
        $circleArray = array(
            'cus_id' => $cus_id,
            'paid_amount' => 0.00,
            'dis_amount' => 0.00,
            'tax_details' => '',
            'payment_method' => '',
            'payment_type' => '',
            'insert_time' => date('Y-m-d H:i:s'),
            'app_date' => $this->user_model->todaydate(),
            'payment_detail_id' => '',
            'd_id' => '',
            'paid_time' => '',
            'paid' => 0,
            'pymntDtlsArray' => ''
        );
        

        $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);
        
    
     //   $tmp_renew = $this->user_model->get_tmp_upgrade($cus_id); 
       
        $new_customer_package_id =$this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");;
        
        $new_subscription_period_id = $new_upgrade_subscription;  

                       
        $new_package_amt = $this->user_model->get_package_subscription_details($new_customer_package_id, $new_subscription_period_id, "package_amount");
        
        
        $tot_payment_amount = $new_package_amt;


        $subscription_period_name = $this->user_model->get_subscription_periods($new_subscription_period_id, "subscription_period_name");
        $cus_package_name = $this->user_model->get_package($new_customer_package_id, "customer_package_name");
        $cus_package_des = $this->user_model->get_package($new_customer_package_id, "customer_package_description");
        

        $old_pack_cost = $this->user_model->get_customer($cus_id, "package_amount");
        $old_cus_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");
        $old_subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
        $usage_days = $this->user_model->get_package_usage($cus_id);
        

        $no_of_days = $this->user_model->get_customer($cus_id, "no_of_days");

        $usage_price = round(($old_pack_cost / $no_of_days), 2) * $usage_days;
        $refund_amt = round(($old_pack_cost - $usage_price), 2);
        $tot_payment_amount -= $refund_amt;


       // $tot_payment_amount = $tmp_renew->row(0)->tot_cus_package_amount;
      //  $tax_array = $tmp_renew->row(0)->tax_array;



        //Previously bought devices.
        // Need to refund for these devices.
        $device_list_ref = $this->user_model->get_device_amt_renew($cus_id);
        

        //Previously bought channels.
        // Need to refund for these channels.
        $chn_amt_ref = $this->user_model->get_channel_amt_renew($cus_id);
//        $tot_payment_amount += $tot_tax_amount;

        

        $is_paied = $this->process_payments($cus_id, $payment_circle_id, $tot_payment_amount, $payment_type, $postArray);
        
        if ($is_paied['err_type']) {
            $jsn_arry = array(
                'err_type' => 1,
                'msg' => 'Purchase successfully done. Thank You!'
            );
            if ($is_paied['dirct_or_card']) {
                $crd_dtil_id = $is_paied['drct_or_dtail_id'];
                $d_id = 0;
            } else {
                $d_id = $is_paied['drct_or_dtail_id'];
                $crd_dtil_id = 0;
            }
            $start_date = $this->user_model->todaydate();
            
            $no_of_free_days = $this->user_model->get_package_subscription_details($new_customer_package_id, $new_subscription_period_id, "no_of_free_days");

            
            $a = $this->user_model->get_package_subscription2($new_customer_package_id, $new_subscription_period_id);

            $b = $a->result_array();
            $no_of_pack_days = $b[0]['no_of_days'];
            $end_date = $this->dateoperations->sum($start_date, 'day', ($no_of_pack_days + $no_of_free_days));
            $data = array(
                'customer_package_id' => $new_customer_package_id,
                'subscription_period_id' => $new_subscription_period_id,
                'package_amount' => $new_package_amt,
                'no_of_days' => $no_of_pack_days,
                'no_of_free_days' => $no_of_free_days,
                'end_date' => $end_date,
                'start_date' => $start_date,
                'cus_package_status' => 1
            );
 

               //stop update
            $this->db->where("cus_id", $cus_id);
            $this->db->update("customer_package_details", $data);


            //Remove all previous devices in both customers_devices table and customers_devices_amount table.
            //Remove all previous devices in both customers_devices table and customers_devices_amount table.

            //New Package Details
            $pymntDtlsArray[] = array(
                'description' => $cus_package_name . " - " . $cus_package_des . " (" . $subscription_period_name . " Subscription)",
                'unique_id' => '',
                'amount' => round(($new_package_amt), 2),
                'qty' => 1,
                'type' => 'package'
            );

            if ($customer_status != 2) {
                //Old Package Refund

                // $pack_refund = $this->user_model->get_upgrade_package($cus_id, "pack_refund");

                $pack_refund=$refund_amt;
              
                $pymntDtlsArray[] = array(
                    'description' => "Package Refund",
                    'unique_id' => '',
                    'amount' => $pack_refund,
                    'qty' => 1,
                    'type' => 'refund'
                );
            }

           

            if ($customer_status != 2) {


//Refun Old Devices START
                foreach ($device_list_ref->result_array() as $r) {
                    $stb_model = $this->user_model->get_device($r['device_id'], "stb_model");



                    if ($stb_model == 0) {
                        $no_of_pack_dev = $this->user_model->get_pack_no_dev_count($new_customer_package_id, $new_subscription_period_id, $r['device_id']);
                        $no_of_log = $this->user_model->get_field('customers_devices', 'no_of_login', array('cus_id' => $cus_id, 'device_id' => $r['device_id']));
                        $free_no_of_log = $this->user_model->get_field('customers_devices', 'free_no_of_login', array('cus_id' => $cus_id, 'device_id' => $r['device_id']));
                        $dev = ($no_of_log - $free_no_of_log) - $no_of_pack_dev;

                        $dvc_usd_prc = ($r['device_amount'] / $no_of_days)* $usage_days;

                        $dvc_rfnd = ($r['device_amount'] - $dvc_usd_prc);
                        $dvc_rfnd_amount = ($r['device_amount'] - $dvc_usd_prc)*$dev;

                    
                        $pymntDtlsArray[] = array(
                            'description' => $r['device_name'] . " Refund: ",
                            'unique_id' => $r['device_id'],
                            'amount' => $dvc_rfnd,
                            'qty' => $dev,
                            'type' => 'refund'
                        );
                    }



//                $this->gm->delete_data_array("stb", array('cus_id' => $cus_id, 'device_id' => $r['device_id']));

                  $tot_payment_amount -= $dvc_rfnd_amount;

                }

            }
           


//Refun Old Devices END 
//Additional Devices bought when upgrading. START

           //Delete Old Devices
            $this->user_model->delete_data("customers_devices", "cus_id", $cus_id);
            $this->user_model->delete_data("customers_devices_amount", "cus_id", $cus_id);
            $this->user_model->delete_data("customer_devices_list", "cus_id", $cus_id);
            
 


         //   $device_list = unserialize(base64_decode($tmp_renew->row(0)->device_list));
            $cd = 0;
            
            $data_cus_stat = array(
                'customer_status' => 1
            );


             /*

            foreach ($device_list as $d => $value) {
                if ($this->user_model->check_data("customers_devices", array("device_id" => $d, "cus_id" => $cus_id)) == 0) {
                    
                    $data = array(
                        'cus_id' => $cus_id,
                        'device_id' => $d,
                        'no_of_login' => $value,
                        'username' => 'System updated'
                    );

                    $this->db->insert("customers_devices", $data);
//                    $unique_addtnl_id = $this->db->insert_id();
                } else {
                    $this->db->set('no_of_login', 'no_of_login +' . $value, FALSE);
                    $this->db->where("cus_id", $cus_id);
                    $this->db->where("device_id", $d);
                    $this->db->update("customers_devices");
                }

//                $data = array(
//                    'cus_id' => $cus_id,
//                    'device_id' => $d,
//                    'no_of_login' => $value,
//                    'username' => 'System updated'
//                );
//                $this->db->insert("customers_devices", $data);

                
                $unique_addtnl_id = $this->db->insert_id();
                $device_amt = $this->user_model->get_device_price($new_customer_package_id, $new_subscription_period_id, $d);
                $device_name = $this->user_model->get_device($d, "device_name");
                $stb = $this->user_model->get_device($d, "stb_model");
                $is_mobile = $this->user_model->get_device($d, "mobile_model");
                for ($i = 0; $i < $value; $i++) {
                    if ($this->user_model->check_data("customers_devices_amount", array("device_id" => $d, "cus_id" => $cus_id)) == 0) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'device_id' => $d,
                            'device_amount' => $device_amt,
                            'username' => 'System updated',
                            'buying_date' => todaydate()
                        );
                        $this->db->insert("customers_devices_amount", $data);
                    }
                }


                // insert into customer_devices_list table
                $check_device_list = $this->user_model->check_data('customer_devices_list', array('cus_id' => $cus_id, 'device_id' => $d));

                if ($check_device_list) {
                    for ($i = 1; $i <= $value; $i++) {
                        $this->db->query("INSERT INTO customer_devices_list (counter_id, cus_id,device_id,paid_free,device_status) "
                                . "SELECT 1 + coalesce((SELECT max(counter_id) FROM customer_devices_list WHERE cus_id='" . $cus_id . "' AND device_id='$d'), 0), '$cus_id','$d','1','1'");
                    }
                } else {
                    for ($i = 1; $i <= $value; $i++) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'device_id' => $d,
                            'counter_id' => $i,
                            'paid_free' => 1,
                            'device_status' => 1
                        );


                        $this->db->insert("customer_devices_list", $data);
                    }
                }



//                print_r($stb);die();
                if ($stb == 1) {
                    $pymntDtlsArray[] = array(
                        'description' => $device_name,
                        'stb' => 1,
                        'unique_id' => $d,
                        'amount' => $device_amt,
                        'qty' => $value,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
                    $pending_stb_cnt = 1;
//                    $data_cus_stat = array(
//                        'customer_status' => 3
//                    );
                } else {
                    $pymntDtlsArray[] = array(
                        'description' => $device_name . " - Lyca TV Application Access",
                        'unique_id' => $d,
                        'amount' => $device_amt,
                        'qty' => $value,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
//                    $data_cus_stat = array(
//                        'customer_status' => 1
//                    );
                }
//                $tot_payment_amount += $device_amt;

                ++$cd;
            }
         */
            


//            $this->db->where("cus_id", $cus_id);
//            $this->db->update("customers", $data_cus_stat);
//Additional Devices bought when upgrading. END
//FREE Devices with package. START  
            
          
      // @@@@@@  not given for tentkotta same devices
          
            
           // $free_device_list = unserialize(base64_decode($this->user_model->get_upgrade_package($cus_id, "free_device_list")));


              $free_device_list_array = $this->user_model->get_data_join('cus_packages_devices','cus_packages_devices.device_id,cus_packages_devices.no_of_items', "devices", "devices.device_id = cus_packages_devices.device_id", "inner", array('customer_package_id' => $new_customer_package_id, "subscription_period_id" => $new_subscription_period_id));


             $free_device_list=array();

             foreach ($free_device_list_array->result_array() as $e ){
                
                  $free_device_list[$e['device_id']]=$e['no_of_items'];
              }


        //  $free_device_list = unserialize(base64_decode("YToxMjp7aToxMjtpOjE7aToxMztpOjE7aToxNDtpOjE7aToxNTtpOjE7aToxNjtpOjE7aToxNztpOjE7aToxODtpOjE7aToxOTtpOjE7aToyMjtpOjE7aToyMztpOjE7aToyOTtpOjE7aTozMTtpOjE7fQ=="));
           
           
 
            if ($free_device_list) {
                $data_cus_stat = array(
                    'customer_status' => 1
                );



                foreach ($free_device_list as $d => $value) {


//                    $data = array(
//                        'cus_id' => $cus_id,
//                        'device_id' => $d,
//                        'no_of_login' => 1,
//                        'username' => 'System updated'
//                    );
//                    $this->db->insert("customers_devices", $data);

                    if ($this->user_model->check_data("customers_devices", array("device_id" => $d, "cus_id" => $cus_id)) == 0) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'device_id' => $d,
                            'no_of_login' => $value,
                            'username' => 'System updated'
                        );
                        $this->db->insert("customers_devices", $data);
//                    $unique_addtnl_id = $this->db->insert_id();
                    } else {
                        $this->db->set('no_of_login', 'no_of_login +' . $value, FALSE);
                        $this->db->where("cus_id", $cus_id);
                        $this->db->where("device_id", $d);
                        $this->db->update("customers_devices");
                    }

                    $unique_addtnl_id = $this->db->insert_id();
                    $device_name = $this->user_model->get_device($d, "device_name");
                    $stb_model = $this->user_model->get_device($d, "stb_model");
                    $is_mobile = $this->user_model->get_device($d, "mobile_model");

                    // insert into customer_devices_list table
                    $check_device_list = $this->user_model->check_data('customer_devices_list', array('cus_id' => $cus_id, 'device_id' => $d));

                    if ($check_device_list) {
                        for ($i = 1; $i <= $value; $i++) {
                            $this->db->query("INSERT INTO customer_devices_list (counter_id, cus_id,device_id,paid_free,device_status) "
                                    . "SELECT 1 + coalesce((SELECT max(counter_id) FROM customer_devices_list WHERE cus_id='" . $cus_id . "' AND device_id='$d'), 0), '$cus_id','$d','0','1'");
                        }
                    } else {


                        for ($i = 1; $i <= $value; $i++) {
                            $data = array(
                                'cus_id' => $cus_id,
                                'device_id' => $d,
                                'counter_id' => $i,
                                'paid_free' => 0,
                                'device_status' => 1
                            );



                            $this->db->insert("customer_devices_list", $data);
                        }
                    }
                    $no_of_pack_dev = $this->user_model->get_pack_no_dev_count($new_customer_package_id, $new_subscription_period_id, $d);


                    if ($stb_model == 1) {


                        $pymntDtlsArray[] = array(
//                            'description' => $device_name . " (Free)",
                            'description' => $device_name,
                            'stb' => 1,
                            'unique_id' => $d,
                            'amount' => 0,
                            'qty' => $no_of_pack_dev,
                            'type' => 'device',
                            'is_mobile' => $is_mobile
                        );
                        $pending_stb_cnt = 1;
//                        $data_cus_stat = array(
//                            'customer_status' => 3
//                        );
                    } else {
                        $pymntDtlsArray[] = array(
//                            'description' => $device_name . " (Free)",
                            'description' => $device_name . " - Lyca TV Application Access",
                            'unique_id' => $d,
                            'amount' => 0,
                            'qty' => $no_of_pack_dev,
                            'type' => 'device',
                            'is_mobile' => $is_mobile
                        );
//                        $data_cus_stat = array(
//                            'customer_status' => 1
//                        );
                    }
                }
                
//                $this->db->where("cus_id", $cus_id);
//                $this->db->update("customers", $data_cus_stat);
            }
            
                   
            
// @@ Insert Radio channels


            $this->user_model->delete_data("customers_radios", "cus_id", $cus_id);
            
            //   $qry = "INSERT INTO customers_radios select $cus_id,radio_id,0,priority from cus_packages_radio_channels where customer_package_id = $new_customer_package_id AND subscription_period_id = $new_subscription_period_id";
//            $this->db->query($qry);
//            
                   
            if ($customer_status != 2) {
                foreach ($chn_amt_ref->result_array() as $r) {
                    $dvc_usd_prc = ($r['channel_amount'] / $no_of_days) * $usage_days;
                    $dvc_rfnd = $r['channel_amount'] - $dvc_usd_prc;
                    $pymntDtlsArray[] = array(
                        'description' => $r['display_name'] . " Refund: ",
                        'unique_id' => $r['channel_id'],
                        'amount' => $dvc_rfnd,
                        'qty' => 1,
                        'type' => 'refund'
                    );
                $tot_payment_amount -= $dvc_rfnd;
                }
            }
            $this->user_model->delete_data("customers_channels", "cus_id", $cus_id);


            //customer package free channels
            $cus_package = $this->user_model->get_data('cus_packages_channels', '*', array('customer_package_id' => $new_customer_package_id, 'subscription_period_id' => $new_subscription_period_id));
            foreach ($cus_package->result_array() as $r) {

                if ($this->user_model->check_data("customers_channels", array("channel_id" => $r['channel_id'], "cus_id" => $cus_id)) == 0) {
                    $data = array(
                        'cus_id' => $cus_id,
                        'channel_id' => $r['channel_id'],
                        'channel_amount' => 0,
                        'priority' => $r['priority']
                    );
                    $this->db->insert("customers_channels", $data);
                } else {
                    $data = array(
                        'channel_amount' => 0,
                        'priority' => $r['priority']
                    );
                    $this->db->where("cus_id", $cus_id);
                    $this->db->where("channel_id", $r['channel_id']);
                    $this->db->update("customers_channels", $data);
                }
            }


             //Newly purchace channel
            //@@@no new channel option for tentkotta      
            
            /*
            foreach ($chn_list as $c => $value) {
                $channl_id_order_arr = explode("|", $c);
                $data = array(
                    'cus_id' => $cus_id,
                    'channel_id' => $c,
                    'channel_amount' => $value,
                    'priority' => 999
                );
                $this->db->insert("customers_channels", $data);
                $ct++;
            }
           */
            


            //channel sub package insert
            //  $channel_pack_list = unserialize(base64_decode($tmp_renew->row(0)->chn_pack_list));
            
            if ($customer_status != 2) {
                $chn_pack_amt = $this->user_model->get_additional_channel_sub_renew($cus_id, $old_cus_package_id, $old_subscription_period_id);

//                exit;

                foreach ($chn_pack_amt as $r) {
                    $dvc_usd_prc = ($r['cus_amount'] / $no_of_days) * $usage_days;
                    $dvc_rfnd = $r['cus_amount'] - $dvc_usd_prc;
                    $pymntDtlsArray[] = array(
                        'description' => $r['sub_package_name'] . " Refund: ",
                        'unique_id' => $r['id'],
                        'amount' => $dvc_rfnd,
                        'qty' => 1,
                        'type' => 'refund'
                    );
                $tot_payment_amount -= $dvc_rfnd;
                }
            }


//            $this->gm->delete_data("customers_channels", "cus_id", $cus_id);
            
            $this->db->delete('customers_channels', array('cus_id' => $cus_id, 'channel_sub_package' => 1));
            $this->db->delete('`customers_channels_sub_package', array('cus_id' => $cus_id));
            
            
            
            
           // @@ no cahnnel sub pacakge option for tentkotta API
    //        $pymntDtlsArray = array();
            /*
            foreach ($channel_pack_list as $d => $value) {
                $res_chnl_pack = $this->user_model->get_data('channels_sub_package', 'id,pack_id,subscription_id,pacCategory,sub_package_name,amount', array('pack_id' => $new_customer_package_id, 'subscription_id' => $new_subscription_period_id, 'id' => $d))->row();
                $res_pack_channels = $this->user_model->get_data('sub_pakages_channel', 'channel_id,priority', array('sub_pack_id' => $d))->result_array();
                foreach ($res_pack_channels as $ch) {
//                $chn_amt_full_ref = $this->gm->get_channel_price($country_id, $cus_package_id, $subscription_period_id, $ch['channel_id']);
                    if ($this->user_model->check_data('customers_channels', array('cus_id' => $cus_id, 'channel_id' => $ch['channel_id'], 'channel_amount > ' => 0)) == 0) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'channel_id' => $ch['channel_id'],
                            'channel_amount' => 0.00,
                            'priority' => $ch['priority'],
                            'channel_sub_package' => 1
                        );
                        $this->db->insert("customers_channels", $data);
                    } else {
                        $data = array(
                            'channel_amount' => 0.00,
                            'priority' => $ch['priority'],
                            'channel_sub_package' => 1
                        );
                        $this->db->where(array('cus_id' => $cus_id, 'channel_id' => $ch['channel_id']));
                        $this->db->update('customers_channels', $data);
                    }
                }



                $chn_pack_amt = $res_chnl_pack->amount;

                $channel_pack_description = $res_chnl_pack->sub_package_name;


                $pymntDtlsArray[] = array(
                    'description' => $channel_pack_description,
                    'unique_id' => $d,
                    'amount' => $chn_pack_amt,
                    'qty' => 1,
                    'type' => 'channel_pack'
                );

                $data_cus_chnl_pack = array(
                    'cus_id' => $cus_id,
                    'sub_pack_id' => $d,
                    'cus_amount' => number_format($value, 2)
                );

                $this->db->insert('customers_channels_sub_package', $data_cus_chnl_pack);
                $tot_paid_amount += $chn_pack_amt;
            }
            */
            //end channel sub package            


            
            
            
            //@@additional charges not for tentkotta api
            
            /*
            
           // $additional_charges = $tmp_renew->row(0)->additional_charges;
            $additional_charges = unserialize(base64_decode($additional_charges));

            //additional charges array
            $arrAdtionalCharge = array();



            foreach ($additional_charges as $d => $value) {
                $c = explode("-", $d);
                $device_id = $c['0'];
                $device_name = $this->gm->get_device($device_id, "device_name");
                $v = explode("-", $value);
                foreach ($v as $charge_id) {
//                    $charge_amt = $this->gm->get_package_charges_amount($new_customer_package_id, $charge_id, $device_id);
                    $charge_amt = $this->gm->get_package_charges_amount_with_sp_id($new_customer_package_id, $charge_id, $device_id, $new_subscription_period_id);
                    $charge_type = $this->gm->get_charges($charge_id, "charge_type");
                    $pymntDtlsArray[] = array(
                        'description' => "$device_name Additional Charges ($charge_type)  ",
                        'unique_id' => $device_id,
                        'amount' => $charge_amt,
                        'qty' => 1,
                        'type' => 'charges'
                    );

                    $arrAdtionalCharge[] = array(
                        'description' => "$device_name-$charge_type",
                        'unique_id' => $charge_id,
                        'amount' => $charge_amt,
                        'qty' => 1,
                        'type' => 'charges'
                    );
//                    $tot_payment_amount += $charge_amt;
                }
            }
            
            */
            
            


           // $free_charges = unserialize(base64_decode($tmp_renew->row(0)->free_charges));

            foreach ($free_charges as $d => $value) {
                $c = explode("-", $d);
                $device_id = $c['0'];
                $charge_ct = $c['1'];
                $device_name = $this->user_model->get_device($device_id, "device_name");
                $v = explode("-", $value);
                foreach ($v as $charge_id) {
                    $charge_amt = $this->user_model->get_package_charges_amount($new_customer_package_id, $charge_id, $device_id);
                    $charge_type = $this->user_model->get_charges($charge_id, "charge_type");
//                $this->gm->payment_details($payment_circle_id, $device_name . "(with package) " . $charge_type, $charge_amt, $device_id . "-" . $charge_id, 9);
                    $pymntDtlsArray[] = array(
                        'description' => $device_name . " (Free) " . $charge_type,
                        'amount' => $charge_amt,
                        'qty' => 1,
                        'type' => 'charges'
                    );
                    $arrAdtionalCharge[] = array(
                        'description' => "$device_name-$charge_type",
                        'unique_id' => $charge_id,
                        'amount' => $charge_amt,
                        'qty' => 1,
                        'type' => 'charges'
                    );

//                    $tot_paid_amount += $charge_amt;
                }
            }
              
            
            $tot_tax_amount = 0;
            $pckg_taxes = $this->user_model->get_tax_details($new_customer_package_id, $country_id, $mg_id);
            $taxArray = array();
//            $tot_payment_amount -= $tot_dis_amount;
            foreach ($pckg_taxes->result_array() as $pt) {
                if ($pt['tax_method']) {
                    $tax_amount = $tot_payment_amount * ($pt['tax_percentage'] / 100);
                    $taxArray[] = array(
                        'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Exclusive)",
                        'amount' => $tax_amount,
                        'percentage' => $pt['tax_percentage'],
                        'method' => $pt['tax_method'],
                    );
                    $tot_tax_amount += $tax_amount;
                } else {
                    $tax_amount = $tot_payment_amount * ($pt['tax_percentage'] / (100 + $pt['tax_percentage']));
                    $taxArray[] = array(
                        'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Inclusive)",
                        'amount' => $tax_amount,
                        'percentage' => $pt['tax_percentage'],
                        'method' => $pt['tax_method'],
                    );
                }
            }


            if ($postArray['payment_method'] == 121 || $postArray['payment_method'] == 122 || $postArray['payment_method'] == 123) {
                $tot_payment_amount = 0;
                $tot_dis_amount = 0;
            }

            
            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => $tot_payment_amount,
                'dis_amount' => $tot_dis_amount,
                'tax_details' => $tax_array,
                'payment_method' => $is_paied['payment_method_type'],
                'payment_type' => $payment_type,
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' => $this->user_model->todaydate(),
                'payment_detail_id' => $crd_dtil_id,
                'd_id' => $d_id,
                'paid_time' => date('Y-m-d H:i:s'),
                'paid' => 1,
                'pymntDtlsArray' => serialize($pymntDtlsArray),
                'pending_stb_cnt' => $pending_stb_cnt,
                'additionalChargesArray' => serialize($arrAdtionalCharge),
                'bank_code' => $bank_code
            );

           


            $this->user_model->payment_circle_log($circleArray, $payment_circle_id, 0);
//            $this->gm->payment_circle_log($cus_id, $payment_type, $tot_payment_amount, $crd_dtil_id, $d_id, 1, 0, $payment_circle_id, $is_paied['transaction_id']);
            if ($is_paied['payment_method_type'] == 27 || $is_paied['payment_method_type'] == 28) {

                if ($is_paied['payment_method_type'] == 27) {
                    $phone_no = $is_paied['phone_no'];
                } else {
                    $phone_no = 0;
                }
                $this->user_model->insert_customer_transaction_table($cus_id, $payment_circle_id, $is_paied['payment_method_type'], $is_paied['responce_arr'], $phone_no);
            }



            
// set customer status according to stb
            if ($customer_status != 2) {         
                $this->user_model->set_cus_status($cus_id);
            } else {
                $opt = $this->user_model->check_cus_stb($cus_id);
              
                if ($opt == 0) {
                    $cus_status = 1;
                } else {
                    $cus_status = 3;
                }
                $data = array(
                    'customer_status' => $cus_status
                );
                $this->user_model->update_data("customers", $data, array('cus_id' => $cus_id));
                $this->user_model->update_cus_last_activated_history($cus_id, 'upgrade');
            }

            

            $this->user_model->customers_log($cus_id, 101, $this->user_model->timenow(), " Has Upgraded His Package", $payment_circle_id);
            $this->user_model->tentkotta_api_log($cus_id, 101, $this->user_model->timenow(), " Has Upgraded His Package", $payment_circle_id);


            $this->user_model->payment_log($payment_circle_id, $crd_dtil_id, 1);
            $data2 = array('cus_id' => $cus_id, 'payment_circle_id' => $payment_circle_id, 'attach' => $payment_circle_id);
            $this->user_model->_send_email("package_upgrade_other", $cus_email, $data2, " Package has upgraded successfully.");
            $data_cus = array(
                'customer_status' => 1,
                'msg' => 'Package Upgraded successfully',
                'rdrctTo' => site_url('customer-package/' . $cus_id)
            );


            /*
             * @ Draw down customer channels, movies, songs, documentries, radios versions as he got upgraded his package
             * 
             */
            // Channel
            $channel_update = array(
                'version_id' => 0
            );
            $this->db->where(array('cus_id' => $cus_id));
            $this->db->update('customer_chanel_update_version', $channel_update);

            // Movies
            $movies_update = array(
                'version_id' => 0
            );
            $this->db->where(array('cus_id' => $cus_id));
            $this->db->update('customer_move_update_version', $movies_update);

            // Songs
            $songs_update = array(
                'version_id' => 0
            );
            $this->db->where(array('cus_id' => $cus_id));
            $this->db->update('customer_songs_update_version', $songs_update);

            // Radios
            $radios_update = array(
                'version_id' => 0
            );
            $this->db->where(array('cus_id' => $cus_id));     
            $this->db->update('customer_radios_update_version', $radios_update);

            // Documentries
            $documentries_update = array(
                'version_id' => 0
            );
            $this->db->where(array('cus_id' => $cus_id));            
            $this->db->update('customer_documentries_update_version', $documentries_update);
                 

            $this->response([                    
                    'status' => REST_Controller::HTTP_CREATED,
                    'message'=>"Package Suceesfully Upgraded",
                ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code


            
        } 


         else {
            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => $tot_payment_amount,
                'dis_amount' => 0.00,
                'tax_details' => $taxArray,
                'payment_method' => $is_paied['payment_method_type'],
                'payment_type' => $payment_type,
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' => todaydate(),
                'payment_detail_id' => $crd_dtil_id,
                'd_id' => $d_id,
                'paid_time' => date('Y-m-d H:i:s'),
                'paid' => 1,
                'pymntDtlsArray' => serialize($pymntDtlsArray)
            );

            $this->user_model->payment_circle_log($circleArray, $payment_circle_id, 0);


            // set customer status according to stb
            $this->user_model->set_cus_status($cus_id);


//            $this->gm->payment_circle_log($cus_id, $payment_type, $tot_payment_amount, $crd_dtil_id, $d_id, 0, 0, $payment_circle_id, $is_paied['transaction_id']);
            $this->user_model->customers_log($cus_id, 15, timenow(), " Payment got faild", $payment_circle_id);
            $this->user_model->tentkotta_api_log($cus_id, 15, timenow(), " Payment got faild", $payment_circle_id);

            $jsn_arry = array(
                'err_type' => 0,
                'msg' => $is_paied['msg'],
//                            'rdrctTo' => site_url("customer-devices/" . $cus_id)
            );
//                        $this->session->set_userdata('err_type', 'warning');
//                        $this->session->set_userdata('err_msg', $is_paied['msg']);
            $this->user_model->delete_data("tmp_customers_upgrade", "cus_id", $cus_id);
                }         
         
            }
         }
    
        else {       
                   $this->response([
                           'status' => REST_Controller::HTTP_BAD_REQUEST,
                           'message' => 'Invalid parameters',
                              ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
           }

       }

    
    
    
     function process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray, $auto_renew = 0) {
        $query = "SELECT currency_code, account_no, cus_title, first_name, created_by, last_name, country_name, lyca_cus, mobile_no, telephone_no, "
                . "fax_no, email, timezone, customer_status, registered_date, promo_type, start_date, end_date, no_of_days, telephone_code "
                . "shrt_frm_cus, Y.* FROM customers C JOIN countries N ON C.country_id = N.country_id "
                . "JOIN payment_circle Y ON (C.cus_id = Y.cus_id AND Y.payment_circle_id = $payment_circle_id) "
                . "LEFT JOIN customer_package_details D ON C.cus_id = D.cus_id";
        
        $cus_details = $this->db->query($query)->row();
        
       // print_r($cus_details); exit();
        
        $cus_status = $cus_details->customer_status;
        if ($cus_status == 1 || $cus_status == 3 || $cus_status == 2 || $cus_status == 0) {
            
            $username = $this->user_model->is_logged();
            if($username==""){
                $username="Cinematix LLC";
            }
            
            $phone_no = $cus_details->mobile_no;
            
            
            
            
          //$country_code = str_replace('+', "", $cus_details->telephone_code);
            $country_code = "GB";

            
            $account_no = $cus_details->account_no;
            $c_code = $country_code;
            $note = '';
            $mob_no_zero = ltrim($phone_no, '0');
            $phone_no = $country_code . $mob_no_zero;


            // enter email address
            $cus_email = $this->user_model->get_customer($cus_id, "email");


            $payment_method_id = $postArray['payment_method'];

            
            if ($payment_method_id == 121 || $payment_method_id == 122 || $payment_method_id == 124) {
                $payment_method_type = 0;
            } else {
                $payment_method_type = $this->user_model->get_payment_method($payment_method_id, "type");
            }

            if ($payment_method_type == 0) {

                $data = array(
                    'cus_id' => $cus_id,
                    'note' => $note,
                    'username' => $username,
                    'payment_method_id' => $payment_method_id
                );
                $this->db->insert("direct_payments", $data);
                $d_id = $this->db->insert_id();

       
                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $d_id,
                        'dirct_or_card' => 0,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment Successfull. Thank You!'
                    );
                    return $jsn_arry;                
            } 
            else {               
                
                $card_type = $postArray['card_type_d'];
                $card_no_1 = $postArray['card_no_1'];
                $card_no_2 = $postArray['card_no_2'];
                $card_no_3 = $postArray['card_no_3'];
                $card_no_4 = $postArray['card_no_4'];
                $card_no = $card_no_1 . $card_no_2 . $card_no_3 . $card_no_4;
                $exp_month = $postArray['exp_month'];
                if ($exp_month < 10) {
                    $exp_month = '0' . $exp_month;
                }
                $exp_year = $postArray['exp_year'];

                $name_on_card = $postArray['name_on_card'];
                $cvc_no = $postArray['cvc_no'];

                if ($postArray['auto_renew'] == 1 AND $paid_amount != 0) {
                    // @@ Get Payment and save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount == 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->wirecard_auth_check($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount != 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } else {
                    // @@ Normal payment
                    $auth_code = $this->gm->wirecard_2($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id);
                }
                if ($auth_code == "0") {
                    $jsn_arry = array(
                        'err_type' => 0,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => '',
                        'dirct_or_card' => 1,
                        'msg' => 'Ooops!!! We could not process your payment.Please contact the bank for more details.Your Account no is ' . $account_no
                    );
                    $log_type = 3;
                    $this->user_model->customers_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);
                    $this->user_model->tentkotta_api_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);

                    return $jsn_arry;
                } else {                  
                    
              //// End of customer_recurring_subscription_details
                    $card_data = array(
                        'cus_id' => $cus_id,
                        'payment_method_id' => $payment_method_id,
                        'card_type' => $card_type,
                        'card_no' => $card_no_1 . " " . $card_no_2 . " " . $card_no_3 . " " . $card_no_4,
                        'name_on_card' => $name_on_card,
                        'cvc_no' => $cvc_no,
                        'expired_from' => $exp_month . "/" . $exp_year,
                        'note' => $note,
                        'primary' => '',
                        'username' => $this->user_model->is_logged(),
                        'last_update' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("customers_payment_details", $card_data);
                    $payment_detail_id = $this->db->insert_id();

                    $data = array(
                        'paid_time' => date('Y-m-d H:i:s'),
                        'paid' => 1,
                        'payment_detail_id' => $payment_detail_id,
                        'auth_code' => $auth_code
                    );
                    $this->user_model->update_data("payment_circle", $data, array('payment_circle_id' => $payment_circle_id));

                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $payment_detail_id,
                        'dirct_or_card' => 1,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment successfull. Thank You!'
                    );
                    
                    
                    return $jsn_arry;
                                      
                }
            }
        } else {
            $jsn_arry = array(
                'err_type' => 0,
                'payment_circle_id' => '',
                'drct_or_dtail_id' => '',
                'dirct_or_card' => '',
                'msg' => 'Customer has not authorized to buy device'
            );
            return $jsn_arry;
        }
    }
}
