<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['accountActivate_post']['limit'] = 100; // 50 requests per hour per user/key
        $this->methods['getUser_post']['limit'] = 100; // 50 requests per hour per user/key 
    }
    

 public function getUser_post()
    {    
     if (!$this->post('email') == "") {
         
          if ($this->user_model->check_data("customers", array('email' => $this->post('email'))) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Email address is Not Valid',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
             $result = $this->user_model->get_data('customers', 'cus_id', array('email' => $this->post('email')))->row();
             $cus_id = $result->cus_id;
               
            if ($this->user_model->check_data("payment_circle", array('cus_id' => $cus_id)) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID is Not Valid',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            
           // $result = $this->user_model->get_data('payment_circle', 'payment_circle_id', array('cus_id' => $cus_id))->row();
            $result = $this->db->query("SELECT payment_circle_id FROM payment_circle WHERE cus_id='$cus_id' ORDER BY payment_circle_id ASC LIMIT 1")->row();
            $user_id = $result->payment_circle_id;
            $cus_data = $this->user_model->get_data('customers', 'email,telephone_no,mobile_no,first_name,last_name', array('cus_id' => $cus_id))->row();
            $start_date = $this->user_model->get_customer($cus_id, "start_date");    
            $cus_status = $this->user_model->get_customer($cus_id, "customer_status");
            $end_date = $this->user_model->get_customer($cus_id, "end_date");
            $country_name = $this->user_model->get_customer($cus_id, "country_name");

             $customer_package_id=$this->user_model->get_data('customer_package_details', 'customer_package_id', array('cus_id' =>$cus_id))->row();
             $subscription_period_id=$this->user_model->get_data('customer_package_details', 'subscription_period_id', array('cus_id' =>$cus_id))->row();
             $devices_arr = $this->user_model->get_data_join('cus_packages_devices', '*', "devices", "devices.device_id = cus_packages_devices.device_id", "inner", array('customer_package_id' => $customer_package_id->customer_package_id, "subscription_period_id" => $subscription_period_id->subscription_period_id, 'status' => 1));
             $devices_with=0;
            foreach ($devices_arr->result_array() as $k) {
               
                if ($k['device_id'] == 19) {
                    $devices_with = 1;
                }
            }

            if ($cus_status == 1) {
                $status = 'Active';
            } elseif ($cus_status == 2) {
                $status = 'expired';
            } elseif ($cus_status == 3) {
                $status = 'STB pending';
            } elseif ($cus_status == 4) {
                $status = 'cancel';
            } elseif ($cus_status == 5) {
                $status = 'notintersted';
            } elseif ($cus_status == 6) {
                $status = 'suspended';
            } elseif ($cus_status == 7) {
                $status = 'shortform';
            } else {
                $status = 'invalide';
            }
            
            
           // $DtlsArray=array();
            
           













            

/*
        $DtlsArray->userId = $user_id;
        $DtlsArray->email = $cus_data->email;
        $DtlsArray->phoneNumber = $cus_data->telephone_no;
        $DtlsArray->firstName = $cus_data->first_name;
        $DtlsArray->lastName = $cus_data->last_name;
        $DtlsArray->country = $country_name;
        $DtlsArray->startDate = $start_date;
        $DtlsArray->status = $status;
        $DtlsArray->subscriptionPeriodId = $subscription_period_id->subscription_period_id;
        $DtlsArray->withStb = $devices_with;*/


              $devices=$this->user_model->get_data('customers_devices', 'device_id,no_of_login', array('cus_id' => $cus_id));
            
             
            foreach ($devices->result_array() as $k) {


                $device_name=$this->user_model->get_data('devices', 'device_name', array('device_id' => $k['device_id']));


                foreach ($device_name->result_array() as $d ){

                $device_name=preg_replace("/[^a-zA-Z]/","",$d['device_name']);



  $free_device_list= $this->user_model->get_data('cus_packages_devices','no_of_items',array("customer_package_id" =>$customer_package_id->customer_package_id, "subscription_period_id" => $subscription_period_id->subscription_period_id,'device_id'=>$k['device_id']))->row();

            

                $additional_dev_count=$k['no_of_login']-$free_device_list->no_of_items;

                if($free_device_list->no_of_items!=""){
                     $free_dev_count=$free_device_list->no_of_items;
                }else{
                    $free_dev_count=0;
                }

                $item_array['free']=(string)$free_dev_count;
                $item_array['additional']=(string)$additional_dev_count;

                $DeviceArray[$device_name]=$item_array;   

                if(($k['device_id']==19)){ 

                     $serilsArray=array();

                   $stb_details=$this->user_model->get_data('stb', 'stb_serial_name,allocated_date,last_update', array('cus_id' =>$cus_id,'device_id' =>19));

                     foreach ($stb_details->result_array() as $i=>$stb){
                         $serilsArray[$i]['serial_no']=$stb['stb_serial_name'];
                         $serilsArray[$i]['allocated_date']=$stb['allocated_date'];
                         $serilsArray[$i]['last_update']=$stb['last_update'];                        
                     }


                    $device_name=preg_replace("/[^a-zA-Z]/","",$d['device_name']);;
                    $DeviceArray[$device_name.'details']=$serilsArray;                           
                   }

                if(($k['device_id']==11)){ 

                     $serilsArray=array();

                   $stb_details=$this->user_model->get_data('stb', 'stb_serial_name,allocated_date,last_update', array('cus_id' =>$cus_id,'device_id' =>11));

                     foreach ($stb_details->result_array() as $i=>$stb){
                          $serilsArray[$i]['serial_no']=$stb['stb_serial_name'];
                         $serilsArray[$i]['allocated_date']=$stb['allocated_date'];
                         $serilsArray[$i]['last_update']=$stb['last_update'];                       
                     }


                   
                    $device_name=preg_replace("/[^a-zA-Z]/","",$d['device_name']);;
                    $DeviceArray[$device_name.'details']=$serilsArray; 

                   }   

                }
            }


            $cus_end_date = $this->user_model->get_customer($cus_id, "end_date");

            $today = $this->user_model->todaydate();              

            $end = strtotime($today);
            $start = strtotime($cus_end_date);
            $expire_time = round(($start - $end) / (60 * 60 * 24), 0);

     
          $DtlsArray= (object) array(
                        'userId' => $user_id,
                        'email' => $cus_data->email,
                        'mobile_no' => $cus_data->mobile_no,
                        'firstName' => $cus_data->first_name,
                        'lastName' => $cus_data->last_name,
                        'country' => $country_name,
                        'startDate' => $start_date,
                        'endDate' => $end_date,
                        'remainingDays' => (string)$expire_time,
                        'status' => $status,
                        'subscriptionPeriodId'=>$subscription_period_id->subscription_period_id,
                        'withStb' => (string)$devices_with,
                        'deviceDetails' =>$DeviceArray
                    );


             if (!is_null($user_id)) {
                 
                   $this->response([
                    'status' => REST_Controller::HTTP_CREATED,
                    'message' => 'Valid UserID',
                      'userDetails' => $DtlsArray


                        ], REST_Controller::HTTP_CREATED);
                 
             }  else {
                  $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID Not matched',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
             }

     } else {
            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameter',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


 public function resetPassword_post()
    {
       if (!$this->post('userId') == "" && !$this->post('email') == "") {
            if ($this->user_model->check_data("customers", array('email' => $this->post('email'))) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Email address is Not valid',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }


            if ($this->user_model->check_data("payment_circle", array('payment_circle_id' => $this->post('userId'))) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID is Not valid',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
            
             $result = $this->user_model->get_data('payment_circle', 'cus_id', array('payment_circle_id' => $this->post('userId')))->row();
             $cus_id = $result->cus_id;
           
            
            if ($this->user_model->check_data("customers", array('cus_id' => $cus_id)) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID does not exist',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
            
           if (!filter_var($this->post('email'), FILTER_VALIDATE_EMAIL)) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid email format',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            } 

            if ($this->user_model->check_data("customers", array('email' => $this->post('email'), 'cus_id' => $cus_id)) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Email address and User ID Not matched',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }



            $email = $this->post('email');
            $password = $this->user_model->reset_random_password();


            $data_cus = array(
                'cus_password' => $password
            );

            $check_data = $this->user_model->update_data('customers', $data_cus, array('cus_id' => $cus_id, 'email' => $email));
            if ($check_data) {
              // $email="thilinaousl@gmail.com";

                $this->user_model->customers_log($cus_id, 11,$this->user_model->timenow(), " was updated customer password.", 0);
                $this->user_model->tentkotta_api_log($cus_id, 11,$this->user_model->timenow(), " was updated customer password.", 0);

                $this->user_model->_send_email("user_pass_request", $email, $cus_id, "Username or Password Request E-mail", 0);
         

                $this->response([
                    'status' => REST_Controller::HTTP_CREATED,
                    'message' => 'Successfully updated',
                    'password' => $password,
                        ], REST_Controller::HTTP_CREATED);
            } else {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Update Failed',
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameter',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    
public function suspend_post() {

        if (!$this->post('userId') == "") {
            if ($this->user_model->check_data("payment_circle", array('payment_circle_id' => $this->post('userId'))) == 0) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID is Not valid',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            $result = $this->user_model->get_data('payment_circle', 'cus_id', array('payment_circle_id' => $this->post('userId')))->row();
            $cus_id = $result->cus_id;

            if ($this->user_model->check_data("customers", array('cus_id' => $cus_id)) == 0) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'User ID does not exist',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }


            //$password = $this->post('password');
            $data_cus = array(
                'customer_status' => 6
            );

            $check_data = $this->user_model->update_data('customers', $data_cus, array('cus_id' => $cus_id));
            if ($check_data) {

                $data = array(
                    'suspended_date' => date('Y-m-d')
                );

                $this->db->where("cus_id", $cus_id);
                $this->db->update("customers", $data);

                $this->user_model->customers_log($cus_id, 11, $this->user_model->timenow(), " was updated customer status into suspend", 0);
                $this->user_model->tentkotta_api_log($cus_id, 11, $this->user_model->timenow(), " was updated customer status into suspend", 0);

                $this->response([
                    'status' => REST_Controller::HTTP_CREATED,
                    'message' => 'Successfully updated',
                        ], REST_Controller::HTTP_CREATED);
            } else {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Update Failed',
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameter',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

 public function accountActivate_post()
    {
        $cus_id=$this->post('userId');
        $serials=$this->post('serials');
     
        
        if ($cus_id == NULL) {
            $this->response([
                'status' => FALSE,
                'message' => 'Invalid parameter',
                'http_code' => REST_Controller::HTTP_BAD_REQUEST,
                ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        if ($this->user_model->check_data("customers", array('cus_id' => $cus_id)) == 0) {
            $this->response([
                'status' => FALSE,
                'message' => 'Invalid parameter',
                'http_code' => REST_Controller::HTTP_BAD_REQUEST,
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

      
        $currency_code = $this->user_model->get_customer($cus_id, "currency_code");
        $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as m_id", "m_id");
        $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
        $cus_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");
        $subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
        $data['devices'] = $this->user_model->get_data("devices", "*", array("mg_id" => $mg_id, 'status' => 1, 'show_on_web' => 1));
        $data['other_charges_devices'] = $this->user_model->other_charges_devices();
        $data['pckg_taxes'] = $this->user_model->get_tax_details($cus_package_id, $country_id, $mg_id);

        $usage_days = $this->user_model->get_package_usage($cus_id);
        $no_of_days = $this->user_model->get_customer($cus_id, "no_of_days");
        $no_of_free_days = $this->user_model->get_customer($cus_id, "no_of_free_days");
        $no_of_days += $no_of_free_days;
        $priceable_days = $no_of_days - $usage_days;

      //  $data['usage_days'] = $usage_days;
      //  $data['no_of_days'] = $no_of_days;
       // $data['priceable_days'] = $priceable_days;

        //$currency_code = $this->gm->get_country($country_id, "currency_code");
       // $data['currency_code'] = $currency_code;
      //  $data['country_id'] = $country_id;

       // $this->load->view('cus_own_device_mgt', $data);
    }

    public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retreival.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post() {
        $pymntDtlsArray = array();
        $DiscountArray = array();
        $taxArray = array();
        $arrAdtionalCharge=array();
        $tot_paid_amount = 0.00;
        $mg_id = 2;



        if ($this->post('country_code') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Country code should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

         $country_id = $this->user_model->get_data('countries', 'country_id', array('country_code' => $this->post('country_code')))->row('country_id');


        if ($this->post('language') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Language should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        if ($this->post('subscription') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Subscription period should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        if ($this->post('first_name') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'First name should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
         if ($this->post('last_name') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Last name should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
         if ($this->post('city') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'City should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
        if ($this->post('postal_code') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Postal code should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
        if ($this->post('postal_code') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Postal code should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
         if ($this->post('mobile_no') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Mobile number should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
         if ($this->post('email') == "") {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Email should  not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }


            $mobile_length = strlen((string)$this->post('mobile_no'));
            $mobile_num = preg_match("/[^0-9]/", $this->post('mobile_no'));
                if ($mobile_num == 1) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Invalid Mobile Number format',
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }
            
            if ($mobile_length > 11) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid Mobile Number',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
            
         
            if (!$this->post('telephone_no') == "") {
                
                $telephone_num = preg_match("/[^0-9]/", $this->post('telephone_no'));
                if ($telephone_num == 1) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Invalid Telephone Number format',
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }

                $telephone_no_length = strlen((string) $this->post('telephone_no'));
                if ($telephone_no_length > 11) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Invalid Telephone Number',
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }
            }
             
            if (!$this->post('fax') == "") {

                $fax_num = preg_match("/[^0-9]/", $this->post('fax'));
                if ($fax_num == 1) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Invalid Fax Number format',
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }

                $fax_no_length = strlen((string) $this->post('fax'));
                if ($fax_no_length > 11) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Invalid Fax Number',
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }
            }


         if ($this->post('with_stb')==NULL) {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'With Stb should not be empty',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

             if(($this->post('with_stb')==1) && $country_id!=455){
                 $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'No allowed Packages'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }


           if($this->post('with_stb')==1){
               $subscriptions_arr=array(10);
            }
            else{
               $subscriptions_arr=array(7,8,9,10);
           }


                     if(!in_array($this->post('subscription'),$subscriptions_arr)){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'No allowed Packages'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
              }
        


        if ($this->post('with_stb') == 1 || $this->post('with_stb')==0) {

            if($this->post('with_stb') == 1){
                
              $with_stb_pack = 601;  
            }else{
               $with_stb_pack = 575;    
            }

            
        }  else {
        $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid With stb',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
        }

        if (!$this->post('country_code') == "" && !$this->post('language') == "" && !$this->post('subscription') == "" && !$this->post('first_name') == "" && !$this->post('last_name') == "" && !$this->post('city') == "" && !$this->post('postal_code') == "" && !$this->post('mobile_no') == "" && !$this->post('email') == "") {
           
            if (!filter_var($this->post('email'), FILTER_VALIDATE_EMAIL)) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid email format',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }           


 if ($this->user_model->check_data("customers", array('email' => $this->post('email'))) > 0) {
                
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Email address already exist',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

            }


 if ($this->user_model->check_data("customers", array('mobile_no' => $this->post('mobile_no'))) > 0) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Mobile Number already exist',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
            


 $timezone = "UTC";
            $agent_id = 1000873;
            $country_code = $this->post('country_code');

            if ($this->user_model->check_data("countries", array('country_code' => $country_code)) == 0) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid country code',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            $country_id = $this->user_model->get_data('countries', 'country_id', array('country_code' => $country_code))->row('country_id');
            $account_no = $this->user_model->get_acc_no($agent_id, $country_id);
            $country_code = $this->user_model->get_data('countries', 'country_code', array('country_id' => $country_id))->row('country_code');
            $currency_code = $this->user_model->get_country($country_id, "currency_code");
            $lang_id = $this->post('language');
            $subscription_period_id = $this->post('subscription');

            if ($this->user_model->check_data("subscription_periods", array('subscription_period_id' => $subscription_period_id)) == 0) {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Not valid subscription periods',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }


            $query = "SELECT * FROM cus_packages C JOIN cus_packages_subscription_periods S ON C.customer_package_id = S.customer_package_id WHERE C.country_id = $country_id AND C.lang_id = $lang_id AND S.subscription_period_id = $subscription_period_id ";
            // $cus_package_id = $this->db->query($query)->row();


            if($country_id == 455) {
               $cus_package_id = $with_stb_pack;
            } elseif ($country_id == 273) {
                $cus_package_id = 584;
            } elseif ($country_id == 248) {
                $cus_package_id = 585;
            } else {
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'No packages available for this country',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }



            $cus_package_name = $this->user_model->get_package($cus_package_id, "customer_package_name");
            $data['no_of_free_device'] = $this->user_model->get_pack_device_ct($cus_package_id, $subscription_period_id);
            $no_of_days = $this->user_model->get_subscription_periods($subscription_period_id, "no_of_days");
            $subscription_period_name = $this->user_model->get_subscription_periods($subscription_period_id, "subscription_period_name");
            $no_of_free_days = $this->user_model->get_package_subscription_details($cus_package_id, $subscription_period_id, "no_of_free_days");
            // $no_of_promo_free_days = $this->gm->get_promo_subscription_freedays($cus_package_id, $promo_id, $subscription_period_id, $country_id, $lang_id);
            $channels_sub_pack = $this->user_model->get_data('channels_sub_package', 'id,pacCategory,sub_package_name,amount', array('pack_id' => $cus_package_id, 'subscription_id' => $subscription_period_id))->result_array();
            $taxes = $this->user_model->get_data_join("cus_packages_taxes", "*", "taxes", "taxes.tax_id = cus_packages_taxes.tax_id", "inner", array("customer_package_id" => $cus_package_id));
          
            $cus_package_amount = round($this->user_model->get_package_subscription_details($cus_package_id, $subscription_period_id, "package_amount"), 2);
           
            //-------------------------devices stb------------------------------
            $free_devices = $this->user_model->get_data_join('cus_packages_devices', '*', "devices", "devices.device_id = cus_packages_devices.device_id", "inner", array('customer_package_id' => $cus_package_id, "subscription_period_id" => $subscription_period_id, 'status' => 1));
            $no_of_free_device = $this->user_model->get_pack_device_ct($cus_package_id, $subscription_period_id);
          
            $cus_package_no_taxt_amount = $this->user_model->get_package_subscription_details($cus_package_id, $subscription_period_id, "package_amount");
          
            $other_charges_devices = $this->user_model->other_charges_devices();
            //-------------------------devices stb------------------------------
            
            $tot_cus_package_amount = $cus_package_amount ;
            $tot_paid_amount = $cus_package_no_taxt_amount ;
            
            $channels= $this->user_model->get_data('channels', '*', array("status" => 1));

             
            $password = $this->user_model->random_password();
            $data = array(
                'mg_id' => 2,
                'country_id' => $country_id,
                'agent_id' => $agent_id,
                'account_no' => $account_no,
                'cus_title' => $this->post('cus_title'),
                'first_name' => $this->post('first_name'),
                'last_name' => $this->post('last_name'),
                'cus_password' => $password,
                'account_type' => 1,
                'postal_code' => $this->post('postal_code'),
                'telephone_no' => $this->post('telephone_no'),
                'mobile_no' => $this->post('mobile_no'),
                'email' => $this->post('email'),
                'city' => $this->post('city'),
                'state' => $this->post('state'),
                'home_no' => $this->post('home_no'),
                'address' => $this->post('address'),
                'language_id' => $lang_id,
                'sub_agent_agent_id' => $agent_id,
                'customer_status' => 1,
                'fax_no' => $this->post('fax'),
                'registered_date' => mdate("%Y-%m-%d", time()),
                'how_did_you_find_us' => 'Tentkotta API',
                'timezone' => "UTC",
                'source_created' => 2,
                'created_by' => $agent_id
            );
            $cus_email=$this->post('email');
            $cus_id = $this->user_model->insert_data_id("customers", $data);

        $value = $this->gen_parental_code();
        $query="INSERT INTO customer_options(cus_id,movie_parental_code) VALUES (".$cus_id.",".$value.")";
        $is_insert =  $this->db->query($query);
           
             if (isset($agent_id) && $agent_id != "") {
                $agent_data = array(
                    'agent_id' => $agent_id,
                    'cus_id' => $cus_id,
                    'sales_time' => date("Y-m-d H:i:s", (time())),
                    'username' => "Cinematix",
                    'sales_status' => 0
                );
                $this->user_model->insert_data("agents_sales", $agent_data);
            }
            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => 0.00,
                'dis_amount' => 0.00,
                'tax_details' => '',
                'payment_method' => 30,
                'payment_type' => '',
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' => $this->user_model->todaydate(),
                'payment_detail_id' => '',
                'd_id' => '',
                'paid_time' => '',
                'paid' => 0,
                'pymntDtlsArray' => '',
                'disc_code' => ''
            );
            
            

            $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);
            if ($cus_package_id != 0) {
            $start_date = $this->user_model->local_date($timezone);
            $end_date = $this->dateoperations->sum($start_date, 'day', ($no_of_days + $no_of_free_days));
                $data = array(
                    'cus_id' => $cus_id,
                    'customer_package_id' => $cus_package_id,
                    'subscription_period_id' => $subscription_period_id,
                    'package_amount' => $cus_package_amount,
                    'no_of_days' => $no_of_days,
                    'no_of_free_days' => $no_of_free_days,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'ds_id' => 0,
                    'cus_package_status' => 1,
                    'promo_free_days' => 0
                );

                $this->db->insert("customer_package_details", $data);
            }

            $pymntDtlsArray[] = array(
                'description' => $cus_package_name . " - (" . $subscription_period_name . " Subscription)",
                'amount' => $cus_package_amount,
                'qty' => 1,
                'type' => 'package'
            );
            
            $tot_dis_amt = 0;
            $promotion_discount=0;
            if ($cus_package_id != 0) {
                $pckg_taxes = $this->user_model->get_tax_details($cus_package_id, $country_id, $mg_id);
             
                foreach ($pckg_taxes->result_array() as $pt) {
                   
                    if ($pt['tax_method']) {
                        $taxAmount = ($tot_paid_amount - $tot_dis_amt) * ($pt['tax_percentage'] / 100);
                        $taxArray[] = array(
                            'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Exclusive)",
                            'amount' => $taxAmount,
                            'percentage' => $pt['tax_percentage'],
                            'method' => $pt['tax_method'],
                            'qty' => 1
                        );
                        $tot_paid_amount += $taxAmount;
                    } else {
                        $taxAmount = round(((($tot_paid_amount - $tot_dis_amt) * $pt['tax_percentage']) / (100 + $pt['tax_percentage'])), 2);
                        $taxArray[] = array(
                            'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Inclusive)",
                            'amount' => $taxAmount,
                            'percentage' => $pt['tax_percentage'],
                            'method' => $pt['tax_method'],
                            'qty' => 1
                        );
                    }
                }
            }
   
        //------------------------------------------------------------------------------------------------------------------------------------------
        foreach ($free_devices->result_array() as $r) {
             $device_id = $r['device_id']; 
             $device_amt = $this->user_model->get_device_price($cus_package_id, $subscription_period_id, $device_id);
             $free_device_array[$device_id] = 1;
            } 


        if ($free_device_array) {
            foreach ($free_device_array as $d => $value) {
                $no_of_login = $this->user_model->get_field("cus_packages_devices", "no_of_items", array("customer_package_id" => $cus_package_id, "subscription_period_id" => $subscription_period_id, "device_id" => $d));
                $data = array(
                    'cus_id' => $cus_id,
                    'device_id' => $d,
                    'no_of_login' => $no_of_login,
                    'username' => 'System updated'
                );
                $this->db->insert("customers_devices", $data);
                $unique_addtnl_id = $this->db->insert_id();
                $device_name = $this->user_model->get_device($d, "device_name");
                $stb = $this->user_model->get_device($d, "stb_model");
                $is_mobile = $this->user_model->get_device($d, "mobile_model");
                if ($stb == 1) {
                    $pymntDtlsArray[] = array(
                        'description' => $device_name,
                        'stb' => 1,
//                      'unique_id' => $d . '-' . $unique_addtnl_id,
                        'unique_id' => $d,
                        'amount' => 0.00,
                        'qty' => $no_of_login,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
                    $stb_pending_count = 1;
                } else {
                    $pymntDtlsArray[] = array(
                        'description' => $device_name . " - Lyca TV Application Access",
                        'unique_id' => $d,
                        'amount' => 0.00,
                        'qty' => $no_of_login,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
                }
            }
        }
              
        
        $cus_pack_songs = $this->user_model->get_data('cus_packages_songs', '*', array('customer_package_id' => $cus_package_id, "subscription_period_id" => $subscription_period_id));
       
        foreach (array_unique($cus_pack_songs->result_array(),SORT_REGULAR) as $s) {

            $data = array(
                'cus_id' => $cus_id,
                'songs_id' => $s['songs_id'],
                'songs_amount' => 0,
                'priority' => $s['priority']
            );
            $this->db->insert("customers_songs", $data);
        }
        // Customers Song Insertion is done - End
        // Insert Customers Documentaries - Start 
        $cus_pack_docs = $this->user_model->get_data('cus_packages_documentries', '*', array('customer_package_id' => $cus_package_id, "subscription_period_id" => $subscription_period_id));

        foreach (array_unique($cus_pack_docs->result_array(),SORT_REGULAR) as $s) {
            
            $data = array(
                'cus_id' => $cus_id,
                'docmntry_id' => $s['documentry_id'],
                'documentry_amount' => 0,
                'priority' => $s['priority']
            );
            $this->db->insert("customers_documentries", $data);
        }
        // Customers Documentary Insertion is done - End
        // @@ Insert ADDITIONAL channels details START 
        if ($cus_package_id != 0) {

            $cus_package = $this->user_model->get_data('cus_packages_channels', '*', array('customer_package_id' => $cus_package_id, "subscription_period_id" => $subscription_period_id));
            foreach ($cus_package->result_array() as $r) {
                $data = array(
                    'cus_id' => $cus_id,
                    'channel_id' => $r['channel_id'],
                    'channel_amount' => 0,
                    'priority' => $r['priority'],
                    'customer_package_id' => $cus_package_id
                );

                $this->db->insert("customers_channels", $data);
            }
        }

            if ($cus_package_id != 0) {
                $this->user_model->customers_log($cus_id, 1, $this->user_model->timenow(), " has joined to the system", $cus_package_id);
                $this->user_model->customers_log($cus_id, 2, $this->user_model->timenow(), " has made the subscription fee", $payment_circle_id);
                $this->user_model->tentkotta_api_log($cus_id, 1, $this->user_model->timenow(), " has joined to the system", $payment_circle_id);
                $this->user_model->tentkotta_api_log($cus_id, 2, $this->user_model->timenow(), " has made the subscription fee", $payment_circle_id);
            }

            $data_direct_pay = array(
                'cus_id' => $cus_id,
                'note' => " ",
                'username' => "Cinematix",
                'payment_method_id' => 30
            );
            $this->db->insert("direct_payments", $data_direct_pay);
            $d_id = $this->db->insert_id();

            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => ($tot_paid_amount - $tot_dis_amt - $promotion_discount),
                'dis_amount' => $tot_dis_amt,
                'discount_details' => serialize($DiscountArray),
                'tax_details' => serialize($taxArray),
                'payment_method' => 30,
                'payment_type' => 1,
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' => $this->user_model->todaydate(),
                'payment_detail_id' => 0,
                'd_id' => $d_id,
                'paid_time' => date("Y-m-d H:i:s"),
                'paid' => 1,
                'pymntDtlsArray' => serialize($pymntDtlsArray),
                'pending_stb_cnt' => $stb_pending_count,
                'additionalChargesArray' => serialize($arrAdtionalCharge)
            );

       
            $this->user_model->payment_circle_log($circleArray, $payment_circle_id, 0);
            $this->user_model->set_cus_status($cus_id);
            if (!is_null($cus_id)) {


                $cus_hash = $this->user_model->sha1_encrypt('sha256', $cus_id, "SJKHjjhJKHAKJ56");
                $update_array = array('encrypted_cus_id' => $cus_hash);
                $this->db->where(array('cus_id' => $cus_id));
                $this->db->update('customers', $update_array);

                //$cus_email="thilinaousl@gmail.com";
                $data2 = array('cus_id' => $cus_id, 'payment_circle_id' => $payment_circle_id, 'subscription_start_date' => $start_date);
               
               // $this->user_model->_send_email("other_payment_1", $cus_email, $data2, "");
                $this->user_model->_send_email("other_payment_lyca_stb", $cus_email, $data2, "");
                $this->user_model->_send_email("email_verification", $cus_email, $data2, "Email address verification", 0);

                $this->response([
                    'status' => REST_Controller::HTTP_CREATED,
                    'message' => 'Successfully Registered',
                    'userId' => $payment_circle_id,
                    'password' => $password,
                        ], REST_Controller::HTTP_CREATED); // BAD_REQUEST (400) being the HTTP response code
                // $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code  
            } else {
                // $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code 
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Failed to add',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }
        } else {

            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameter',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            //  $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    function gen_parental_code() {
        $digits = 4;
            $value = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
           // if ($this->gm->check_data("customer_options", array('movie_parental_code' => $value)) == 0) {
            return $value;
         //   }
    }
    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0) {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

      
     public function report_post()
    {
    if (!$this->post('startDate') == "" && !$this->post('endDate') == "") {
        
        $startDate= $this->post('startDate');
        $endDate = $this->post('endDate');
        
             $run_query = "SELECT
                            customers.cus_id,
                            customers.country_id,
                            countries.country_id,
                            countries.country_name,
                            customers.*,
                            customer_package_details.end_date as end_d,
                            customer_package_details.start_date as start_d,
                              customer_package_details.*,
                            customer_tentkotta_package_details.*
                          FROM customers
                            LEFT OUTER JOIN countries
                              ON customers.country_id = countries.country_id
                            LEFT OUTER JOIN customer_package_details
                              ON customers.cus_id = customer_package_details.cus_id
                            LEFT OUTER JOIN customer_tentkotta_package_details
                              ON customers.cus_id = customer_tentkotta_package_details.cus_id WHERE customers.registered_date >= '$startDate' AND customers.registered_date <= '$endDate' AND customers.source_created = 2 ";
                $customer_details = $this->db->query($run_query)->result_array();
            
               if($customer_details==NULL){
                   $this->response([
                    'status' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'No users were found',
                        ], REST_Controller::HTTP_NOT_FOUND); // BAD_REQUEST (400) being the HTTP response code 
               } 
                
        foreach ($customer_details as $r) {
            
           
                if ($r["status"] == 1) {
                    $status = 'Active';
                } elseif ($r["status"] == 1) {
                    $status = 'Unsubscribed';
                }  else {
                    $status = 'Invalid';
                }
                if ($r["activation_status"] == 1) {
                    $activation_status = 'Device activated';
                } elseif ($r["activation_status"] == 2) {
                    $activation_status = 'Invalid';
                }  else {
                    $activation_status = 'Activated pending';
                }


                $DtlsArray[] = array(
                        'email' => $r["email"],
                        'phoneNumber' => $r["telephone_no"],
                        'mobileNumber' => $r["mobile_no"],
                        'firstName' => $r["first_name"],
                        'lastName' => $r["last_name"],
                        'country' => $r["country_name"],
                        'subscriptionCreatedDate' => $r["start_d"],
                        'subscriptionEndDate' => $r["end_d"],
                        'activationDate' => $r["activation_date"],
                        'unsubscribedDate' => $r["unsubscribed_date"],
                        'status' => $activation_status
                    );
  
        }
              
                  $this->response([
                    'status' => REST_Controller::HTTP_CREATED,
                    'message' => 'Report generated successfully',
                    'report' => $DtlsArray,
                        ], REST_Controller::HTTP_CREATED);

        } else {
            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameter',
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



}