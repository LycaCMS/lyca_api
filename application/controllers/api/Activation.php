<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Activation extends REST_Controller {

    protected $methods = [
          'box_activation_post' => ['level' => 10],
        ];




function box_activation_post() {
    
        $user_id = $this->post("userId");        
        $devices=$this->post("serials"); 
        
        $devices_count=count($devices);      

         $allowed_type=strtolower($this->input->server('CONTENT_TYPE'));


         if(!is_array($devices)){
             $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid parameters',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
                }  


         if($allowed_type!="application/json"){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Only the content type application/json is allowed'
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }
        
         if (!$user_id=="" && !$devices_count==0) {
           
        if ($this->user_model->check_data("payment_circle", array('payment_circle_id' =>$user_id)) < 1) {
                
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'This Customer Do not have Registered Account',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
                }                
         
                        
   
             else {
                 
                                  
                    $cus_id=$this->user_model->get_data('payment_circle','*', array('payment_circle_id' =>$user_id))->row()->cus_id;
                 
                    $check_cus_status=$this->user_model->get_data('customers','*', array('cus_id' => $cus_id))->row()->customer_status;
                   
                    $allocated=0;
                   
                    if( ($check_cus_status!=1) && ($check_cus_status!=3)){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Roku Registration is not allowed'
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }


                     
                    if($devices_count>3){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Maximum number of Serials Exceeded'
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }
 
                    
                    foreach ($devices as $dev) {


                                         
                    if ($this->user_model->check_data("stb", array('stb_serial_name' =>$dev)) > 0){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'serial_no' => $dev,
                            'message' => 'Serial is Already Exist'
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                         }
                    }
                    
                    foreach ($devices as $dev) {        
                        if(strlen($dev)<12 || (preg_match('/[^a-z_\-0-9]/i', $dev)) )
                            
                          $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'serial_no' => $dev,
                            'message' => 'Invalid Serial Length or Format'
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }   
                  
                 
                   
        $mg_id = 2;
        $payment_type = 3;
        $pending_stb_cnt = 0;
        $dev_id=11;
        $postArray['payment_method']=31;
        $paid_amount=0;

       
        $circleArray = array(
            'cus_id' => $cus_id,
            'paid_amount' => 0.00,
            'dis_amount' => 0.00,
            'tax_details' => '',
            'payment_method' => $postArray['payment_method'],
            'payment_type' => '',
            'insert_time' => date('Y-m-d H:i:s'),
            'app_date' => date('Y-m-d'),
            'payment_detail_id' => '',
            'd_id' => '',
            'paid_time' => '',
            'paid' => 0,
            'pymntDtlsArray' => ''
        );
        
          
        $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);
    
        
        $new_customer_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");
        $new_subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
       
        $usage_days = $this->user_model->get_package_usage($cus_id);
        $no_of_days = $this->user_model->get_customer($cus_id, "no_of_days");
        $no_of_free_days = $this->user_model->get_customer($cus_id, "no_of_free_days");
        $no_of_days += $no_of_free_days;
        $priceable_days = $no_of_days - $usage_days;
        $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
        $str = "";
        
     
        $is_paied = $this->process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray);
        
        $is_paied['err_type']=1;
        
        if ($is_paied['err_type']) {
            
             if ($is_paied['dirct_or_card']) {
                $crd_dtil_id = $is_paied['drct_or_dtail_id'];
                $d_id = 0;
            } else {
                $d_id = $is_paied['drct_or_dtail_id'];
                $crd_dtil_id = 0;
            }
            
            $no_of_devices=$devices_count;
            
            
            $check_dev_available = $this->user_model->check_data('customers_devices', array('cus_id' => $cus_id, 'device_id' => $dev_id));

            $dev_count_available = $this->user_model->get_data('customers_devices','*', array('cus_id' => $cus_id, 'device_id' => $dev_id))->row();
    
            
            if($check_dev_available){
                
                    $data = array(
                        'cus_id' => $cus_id,
                        'device_id' => $dev_id,
                        'no_of_login' => $dev_count_available->no_of_login + $no_of_devices,
                        'username' => 'System updated'
                    );
               
                
                    $this->db->update("customers_devices", $data,array('device_id' => $dev_id,'cus_id' => $cus_id));

            }
            else {
                
                    $data = array(
                        'cus_id' => $cus_id,
                        'device_id' => $dev_id,
                        'no_of_login' =>$devices_count,
                        'username' => 'System updated'
                    );                
                
                    $this->db->insert("customers_devices", $data);

            }
               
            

            $pymntDtlsArray = array();
            $DiscountArray = array();
            $stbModls = 0;
            
            
            foreach ($devices as $dev) {              
                
                
                $check_dev_available = $this->user_model->check_data('customers_devices', array('cus_id' => $cus_id, 'device_id' => $dev_id));
                
                $device_amt = $this->user_model->get_device_price($new_customer_package_id, $new_subscription_period_id, $dev_id);

                
                for ($j = 0; $j < $no_of_devices; $j++) {
                    if ($this->user_model->check_data('customers_devices_amount', array('cus_id' => $cus_id, 'device_id' => $dev_id)) == 0) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'device_id' => $dev_id,
                            'device_amount' => $device_amt,
                            'username' => 'System updated',
                            'buying_date' => $this->user_model->todaydate()
                        );
                        $this->db->insert("customers_devices_amount", $data);
                    }
                }
            }
                $device_amt = $this->user_model->get_device_price($new_customer_package_id, $new_subscription_period_id, $dev_id);

                
                // insert into customer_devices_list table
                $check_device_list = $this->user_model->check_data('customer_devices_list', array('cus_id' => $cus_id, 'device_id' => $dev_id));

                if ($check_device_list) {
                    for ($i = 1; $i <= $no_of_devices; $i++) {
                        $this->db->query("INSERT INTO customer_devices_list (counter_id, cus_id,device_id,paid_free,device_status) "
                                . "SELECT 1 + coalesce((SELECT max(counter_id) FROM customer_devices_list WHERE cus_id='" . $cus_id . "' AND device_id='$dev_id'), 0), '$cus_id','$dev_id','1','1'");
                    }
                } else {
                    for ($i = 1; $i <= $no_of_devices; $i++) {
                        $data = array(
                            'cus_id' => $cus_id,
                            'device_id' => $dev_id,
                            'counter_id' => $i,
                            'paid_free' => 1,
                            'device_status' => 1
                        );


                        $this->db->insert("customer_devices_list", $data);
                    }
                }


                $device_name = $this->user_model->get_device($dev_id, "device_name");
                $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as mid", "mid");
                $stb_model = $this->user_model->get_field('devices', 'stb_model', array('mg_id' => $mg_id, 'status' => 1, 'device_id' => $dev_id));
                $is_mobile = $this->user_model->get_field('devices', 'mobile_model', array('mg_id' => $mg_id, 'device_id' => $dev_id));
                if ($stb_model == 0) {
                    ++$stbModls;
                    $device_amt_new = (($device_amt / $no_of_days) * $priceable_days);
//                    if ($device_amt_new < $device_amt) {
//                        $device_description = $device_name . ' ((' . $device_amt . '/' . $no_of_days . ')x' . $priceable_days . '=' . number_format((($device_amt / $no_of_days) * $priceable_days), 2);
//                    } else {
                    $device_description = $device_name;
//                    }
                    $pymntDtlsArray[] = array(
                        'description' => $device_description . " - Lyca TV Application Access",
                        'unique_id' => $dev_id,
                        'amount' => $device_amt_new,
                        'qty' => $no_of_devices,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
                } else {
                    $device_description = $device_name;
                    $pymntDtlsArray[] = array(
                        'description' => $device_description,
                        'stb' => 1,
                        'unique_id' => $dev_id,
                        'amount' => $device_amt,
                        'qty' => $no_of_devices,
                        'type' => 'device',
                        'is_mobile' => $is_mobile
                    );
//                    $pending_stb_cnt = 1;
                    $device_amt_new = $device_amt;
                }



//                $this->gm->payment_details($payment_circle_id, $device_name, $device_amt, $dev_id, $payment_type, $no_of_devices);
//                $str .= $no_of_devices . " " . $device_name . ",";
            

          //  $customers_stb_devices = unserialize(base64_decode($this->user_model->get_field('tmp_customers_devices', 'customer_stb_devices', array('cus_id' => $cus_id))));
            $customers_stb_devices = $devices;

            
            $cus_stb_dev_dsnt = array();
            $cus_stb_dev_desc = array();

          //  if (trim($customers_stb_devices[0]['serial'][0]) != '') {
            
                $agent_id = $this->user_model->get_customer($cus_id, "customers.agent_id as agent", "agent");
                $sub_agent_agent_id = $this->user_model->get_customer($cus_id, "sub_agent_agent_id");
                if ($agent_id == $sub_agent_agent_id) {
                    $agent_id = 0;
                }
                $username = $this->user_model->is_logged();
                
                  if($username==""){
                         $username="Cinematix LLC";
                    }
               
                    



                  
                    
                foreach ($devices as $stb_serial_name) {
                    $device_name = $this->user_model->get_device($dev_id, "device_name");
                    $cus_stb_dev_dsnt = array(
                        'description' => $device_name . " Customer device discount",
                        'unique_id' => $dev_id,
//                      'amount' => ($csd['discount_per_dev'] * $csd['dev_count']),


                        'amount' => $device_amt,
                        'qty' => $no_of_devices,
                        'type' => 'stb_discount',
                        'is_mobile' => $is_mobile



                    );                    


                  } 
                    
//                    $tot_paid_amount -= $csd['discount_per_dev'] * $csd['dev_count'];
                    $pymntDtlsArray[] = $cus_stb_dev_dsnt;
                    $DiscountArray[] = $cus_stb_dev_dsnt;

//                    }
                                       
                    foreach ($devices as $stb_serial_name) {
 
                     
                        $query = "SELECT MAX(set_top_box_no) AS set_top_box FROM stb WHERE set_top_box_no BETWEEN 35598 AND 50000";
                        $sql_last_box = $this->db->query($query)->row('set_top_box');
                        $new_box = ++$sql_last_box;
                        
                  //echo "Testing =>".$set_up_box;die();
                        $data_stb = array(
                            'mg_id' => $mg_id,
                            'set_top_box_no' => $new_box,
                            'cus_id' => $cus_id,
                            'country_id' => $country_id,
                            'agent_id' => $sub_agent_agent_id,
                            'sub_agent_id' => $agent_id,
                            'device_id' => $dev_id,
                            'stb_serial_name' => $stb_serial_name,
                            'payment_circle_id' => $payment_circle_id,
                            'config_date' => date('Y-m-d H:i:s'),
                            'allocated_date' => date('Y-m-d H:i:s'),
                            'username' => $username,
                            'last_update' => date('Y-m-d H:i:s'),
                            'physical_device' => 1,
                            'device_status' => 3,
                        );


                        $stb_insrtion=$this->user_model->insert_data("stb", $data_stb);                        
                
                        $this->user_model->customers_log($cus_id, 202, $this->user_model->timenow(), " allocated STB Box", $payment_circle_id);
						$this->user_model->tentkotta_api_log($cus_id, 202, $this->user_model->timenow(), " allocated STB Box", $payment_circle_id);


                        $this->user_model->add_xml($dev);

                        $data = array(
                            'cus_id' => $cus_id,
                            'serial' => $stb_serial_name,
                            'device_id' => $dev_id
                        );
                        $this->db->insert("apple_devices_serial", $data);

                        $pending_stb_cnt = 0;
                        
                    }
             //   }
           // }

           
            /*        
            $pckg_taxes = $this->user_model->get_tax_details($new_customer_package_id, $country_id, $mg_id);
            $taxArray = array();
            foreach ($pckg_taxes->result_array() as $pt) {
                if ($pt['tax_method']) {
                    $taxAmount = ($device_amount + $totalAdditionalChrg) * ($pt['tax_percentage'] / 100);
                    $taxArray[] = array(
                        'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Exclusive)",
                        'amount' => $taxAmount,
                        'percentage' => $pt['tax_percentage'],
                        'method' => $pt['tax_method'],
                    );
//                    $this->gm->payment_details($payment_circle_id, $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Exclusive)", $taxAmount, $pt['tax_id'], 5);
                } else {
                    $taxAmount = ($device_amount + $totalAdditionalChrg) * ($pt['tax_percentage'] / (100 + $pt['tax_percentage']));
                    $taxArray[] = array(
                        'description' => $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Inclusive)",
                        'amount' => $taxAmount,
                        'percentage' => $pt['tax_percentage'],
                        'method' => $pt['tax_method'],
                    );
//                    $this->gm->payment_details($payment_circle_id, $pt['tax_type'] . " " . $pt['tax_percentage'] . "% (Inclusive)", $taxAmount, $pt['tax_id'], 5);
                }
            }
           */


            if ($postArray['payment_method'] == 121 || $postArray['payment_method'] == 122 || $postArray['payment_method'] == 123 || $postArray['payment_method'] ==31) {
                $paid_amount = 0;
            }
            
            $taxArray = array();
            $arrAdtionalCharge="";
            
            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => $paid_amount,
                'dis_amount' => 0.00,
                'discount_details' => serialize($DiscountArray),
                'tax_details' => serialize($taxArray),
                'payment_method' =>$is_paied['payment_method_type'],
                'payment_type' =>$payment_type,
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' => $this->user_model->todaydate(),
                'payment_detail_id' => $crd_dtil_id,
                'd_id' => $d_id,
                'paid_time' => date('Y-m-d H:i:s'),
                'paid' => 1,
                'pymntDtlsArray' => serialize($pymntDtlsArray),
                'pending_stb_cnt' => $pending_stb_cnt,
                'additionalChargesArray' => serialize($arrAdtionalCharge),
                'bank_code' => ''
            );

            $this->user_model->payment_circle_log($circleArray, $payment_circle_id, 0);

            if ($is_paied['payment_method_type'] == 27 || $is_paied['payment_method_type'] == 28) {

                if ($is_paied['payment_method_type'] == 27) {
                    $phone_no = $is_paied['phone_no'];
                } else {
                    $phone_no = 0;
                }
                $this->user_model->insert_customer_transaction_table($cus_id, $payment_circle_id, $is_paied['payment_method_type'], $is_paied['responce_arr'], $phone_no);
            }
            
            $allocated=$this->user_model->customers_log_return($cus_id, 8, $this->user_model->timenow(), " has added Devices to current package successfully.", $payment_circle_id);
            $this->user_model->tentkotta_api_log($cus_id, 8, $this->user_model->timenow(), " has added Devices to current package successfully.", $payment_circle_id);
    
            $data2 = array('cus_id' => $cus_id, 'payment_circle_id' => $payment_circle_id);
            
            $this->user_model->set_cus_status($cus_id);

             
              if($allocated==1){
                $this->response([                    
                    'status' => REST_Controller::HTTP_CREATED,
                    'message'=>"Customer Roku Devices Has Been Added",
                ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code     
            }
            else{
                $this->response([                    
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message'=>"Failed to add Customer Roku Devices",
                ], REST_Controller::HTTP_BAD_REQUEST); // CREATED (201) being the HTTP response code 
            }
                          
                        
            }  
        
       }
        
    }
 else {       
            $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid parameters',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        
    }
    
}
    
    
    
    
     function process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray, $auto_renew = 0) {
        $query = "SELECT currency_code, account_no, cus_title, first_name, created_by, last_name, country_name, lyca_cus, mobile_no, telephone_no, "
                . "fax_no, email, timezone, customer_status, registered_date, promo_type, start_date, end_date, no_of_days, telephone_code "
                . "shrt_frm_cus, Y.* FROM customers C JOIN countries N ON C.country_id = N.country_id "
                . "JOIN payment_circle Y ON (C.cus_id = Y.cus_id AND Y.payment_circle_id = $payment_circle_id) "
                . "LEFT JOIN customer_package_details D ON C.cus_id = D.cus_id";
        $cus_details = $this->db->query($query)->row();
        
       // print_r($cus_details); exit();
        
        $cus_status = $cus_details->customer_status;
        if ($cus_status == 1 || $cus_status == 3 || $cus_status == 2 || $cus_status == 0) {
            
            $username = $this->user_model->is_logged();
            if($username==""){
                $username="Cinematix LLC";
            }
            
            $phone_no = $cus_details->mobile_no;
            
            
            
            
          //  $country_code = str_replace('+', "", $cus_details->telephone_code);
            $country_code = "GB";

            
            $account_no = $cus_details->account_no;
            $c_code = $country_code;
            $note = '';
            $mob_no_zero = ltrim($phone_no, '0');
            $phone_no = $country_code . $mob_no_zero;


            // enter email address
            $cus_email = $this->user_model->get_customer($cus_id, "email");


            $payment_method_id = $postArray['payment_method'];

            
            if ($payment_method_id == 121 || $payment_method_id == 122 || $payment_method_id == 124) {
                $payment_method_type = 0;
            } else {
                $payment_method_type = $this->user_model->get_payment_method($payment_method_id, "type");
            }

            if ($payment_method_type == 0) {

                $data = array(
                    'cus_id' => $cus_id,
                    'note' => $note,
                    'username' => $username,
                    'payment_method_id' => $payment_method_id
                );
                $this->db->insert("direct_payments", $data);
                $d_id = $this->db->insert_id();

       
                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $d_id,
                        'dirct_or_card' => 0,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment Successfull. Thank You!'
                    );
                    return $jsn_arry;                
            } 
            else {               
                
                $card_type = $postArray['card_type_d'];
                $card_no_1 = $postArray['card_no_1'];
                $card_no_2 = $postArray['card_no_2'];
                $card_no_3 = $postArray['card_no_3'];
                $card_no_4 = $postArray['card_no_4'];
                $card_no = $card_no_1 . $card_no_2 . $card_no_3 . $card_no_4;
                $exp_month = $postArray['exp_month'];
                if ($exp_month < 10) {
                    $exp_month = '0' . $exp_month;
                }
                $exp_year = $postArray['exp_year'];

                $name_on_card = $postArray['name_on_card'];
                $cvc_no = $postArray['cvc_no'];

                if ($postArray['auto_renew'] == 1 AND $paid_amount != 0) {
                    // @@ Get Payment and save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount == 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->wirecard_auth_check($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount != 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } else {
                    // @@ Normal payment
                    $auth_code = $this->gm->wirecard_2($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id);
                }
                if ($auth_code == "0") {
                    $jsn_arry = array(
                        'err_type' => 0,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => '',
                        'dirct_or_card' => 1,
                        'msg' => 'Ooops!!! We could not process your payment.Please contact the bank for more details.Your Account no is ' . $account_no
                    );
                    $log_type = 3;
                    $this->user_model->customers_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);
				    $this->user_model->tentkotta_api_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id); 

                    return $jsn_arry;
                } else {                  
                    
              //// End of customer_recurring_subscription_details
                    $card_data = array(
                        'cus_id' => $cus_id,
                        'payment_method_id' => $payment_method_id,
                        'card_type' => $card_type,
                        'card_no' => $card_no_1 . " " . $card_no_2 . " " . $card_no_3 . " " . $card_no_4,
                        'name_on_card' => $name_on_card,
                        'cvc_no' => $cvc_no,
                        'expired_from' => $exp_month . "/" . $exp_year,
                        'note' => $note,
                        'primary' => '',
                        'username' => $this->user_model->is_logged(),
                        'last_update' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("customers_payment_details", $card_data);
                    $payment_detail_id = $this->db->insert_id();

                    $data = array(
                        'paid_time' => date('Y-m-d H:i:s'),
                        'paid' => 1,
                        'payment_detail_id' => $payment_detail_id,
                        'auth_code' => $auth_code
                    );
                    $this->user_model->update_data("payment_circle", $data, array('payment_circle_id' => $payment_circle_id));

                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $payment_detail_id,
                        'dirct_or_card' => 1,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment successfull. Thank You!'
                    );
                    
                    
                    return $jsn_arry;
                                      
                }
            }
        } else {
            $jsn_arry = array(
                'err_type' => 0,
                'payment_circle_id' => '',
                'drct_or_dtail_id' => '',
                'dirct_or_card' => '',
                'msg' => 'Customer has not authorized to buy device'
            );
            return $jsn_arry;
        }
    }
}
