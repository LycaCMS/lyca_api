<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Renew1 extends REST_Controller {

    protected $methods = [
          'box_activation_post' => ['level' => 10],
        ];




function renew_post() {
    
        $user_id = $this->post("userId");        
       
        
         if (!$user_id==""  && !(preg_match('/[^a-z_\-0-9]/i', $user_id))) {
           
        if ($this->user_model->check_data("payment_circle", array('payment_circle_id' =>$user_id)) < 1) {
                
                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'This Customer Do not have Registered Account',
                       ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
                }                
         
                        
   
             else {               

                                  
                    $cus_id=$this->user_model->get_data('payment_circle','*', array('payment_circle_id' =>$user_id))->row()->cus_id;
                    
                    $cus_end_date = $this->user_model->get_customer($cus_id, "end_date");

                    $today = $this->user_model->todaydate();              

                     $end = strtotime($today);
                     $start = strtotime($cus_end_date);
                     $expire_time = round(($start - $end) / (60 * 60 * 24), 0);

                    /*
                     if($expire_time>7){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Cannot renew package before 7 days of expiration'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }
                    */
                 
                    $check_cus_status=$this->user_model->get_data('customers','*', array('cus_id' => $cus_id))->row()->customer_status;
                   
                   
                    if( ($check_cus_status!=1) && ($check_cus_status!=3) && ($check_cus_status!=2)){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Customer is not allowed for Renewal'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                    
         /*           
        $mg_id = 2;
        $payment_type = 3;
        $pending_stb_cnt = 0;
        $dev_id=19;
        $postArray['payment_method']=30;
        
        $charge_types = "Customer Collect";
        $tot_cus_package_amount=0;

        
        
        $new_customer_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");
        $new_subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
       
        $usage_days = $this->user_model->get_package_usage($cus_id);
        $no_of_days = $this->user_model->get_customer($cus_id, "no_of_days");
        $no_of_free_days = $this->user_model->get_customer($cus_id, "no_of_free_days");
        $no_of_days += $no_of_free_days;
        $priceable_days = $no_of_days - $usage_days;
        $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
        $str = "";
        
     //   $device_amt = $this->user_model->get_device_price($new_customer_package_id, $new_subscription_period_id, $dev_id);

        $device_amt=0;
        
        $device_amt = $this->user_model->get_device_price($new_customer_package_id, $new_subscription_period_id, $dev_id);
                       
        $device_amt = $device_amt*$devices_count;
                                
        $paid_amount=$device_amt;

        
        $circleArray = array(
            'cus_id' => $cus_id,
            'paid_amount' => $paid_amount,
            'dis_amount' => 0.00,
            'tax_details' => '',
            'payment_method' => $postArray['payment_method'],
            'payment_type' => '',
            'insert_time' => date('Y-m-d H:i:s'),
            'app_date' => date('Y-m-d'),
            'payment_detail_id' => '',
            'd_id' => '',
            'paid_time' => '',
            'paid' => 0,
            'pymntDtlsArray' => ''
        );
        
          
        $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);
            
     
        $is_paied = $this->process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray);
        
        
        */
        
        
        $postArray['payment_method']=30;
            
      //  $is_paied['err_type']=1;
        
        
    
      //  $cus_id = $this->input->post("cus_id");
        
      //  $postArray = $this->input->post();
        $bank_code = "";
        if ($this->input->post("bank_ref_code") != '') {
            $bank_code = $this->input->post("bank_ref_code");
        }

//        $customer_status = $this->gm->get_customer($cus_id, "customer_status");

        $send_name = $this->user_model->get_customer($cus_id, "first_name");
        $account_no = $this->user_model->get_customer($cus_id, "account_no");
        $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
        $cus_email = $this->user_model->get_customer($cus_id, "email");
        $cus_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cp_id", "cp_id");
        $sp_id = $this->user_model->get_customer($cus_id, "subscription_period_id");

        //$cus_package_amount = $this->gm->get_customer($cus_id, "package_amount");
        $customer_status = $this->user_model->get_customer($cus_id, "customer_status");
        $package_amount_res = $this->user_model->get_data("cus_packages_subscription_periods", "renewal_amount, renewal_free_days", array('customer_package_id' => $cus_package_id, 'subscription_period_id' => $sp_id))->row();
         

        $cus_package_amount = $package_amount_res->renewal_amount;

        $tot_payment_amount = $cus_package_amount;
        $payment_type = 5;
        $pymntDtlsArray = array();
        $pending_stb_cnt = 0;


        $circleArray = array(
            'cus_id' => $cus_id,
            'paid_amount' => 0.00,
            'dis_amount' => 0.00,
            'tax_details' => '',
            'payment_method' => '',
            'payment_type' => '',
            'insert_time' => date('Y-m-d H:i:s'),
            'app_date' => $this->user_model->todaydate(),
            'payment_detail_id' => '',
            'd_id' => '',
            'paid_time' => '',
            'paid' => 0,
            'pymntDtlsArray' => ''
        );
        $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);

//        $payment_circle_id = $this->gm->payment_circle_log($cus_id, '', '0.00', '', '', 0, 1);
//        echo $tot_payment_amount;         
        $no_of_days = $this->user_model->get_customer($cus_id, "no_of_days");
//        echo $this->db->last_query();
        $today = $this->user_model->todaydate();
        $end = strtotime($today);
        $end_date1 = $this->user_model->get_customer($cus_id, "end_date");
        $start = strtotime($end_date1);
        $no_of_rest_days = round(($start - $end) / (60 * 60 * 24), 0) > 0 ? round(($start - $end) / (60 * 60 * 24), 0) : 0;
        $no_of_free_days = $package_amount_res->renewal_free_days;
        $cus_package_name = $this->user_model->get_package($cus_package_id, "customer_package_name");
        $subscription_period_name = $this->user_model->get_subscription_periods($sp_id, "subscription_period_name");

        $pymntDtlsArray[] = array(
            'description' => $cus_package_name . " - (" . $subscription_period_name . " Subscription)",
            'unique_id' => '',
            'amount' => $cus_package_amount,
            'qty' => 1,
            'type' => 'package'
        );
        

// get device amount by packages_devices - Raj sir said that
        $join_condition = 'customers_devices_amount.device_id = packages_devices.device_id';
        $where_clause = array('packages_devices.customer_package_id' => $cus_package_id, 'packages_devices.subscription_period_id' => $sp_id, 'customers_devices_amount.cus_id' => $cus_id);
        $device_amt = $this->user_model->get_data_join('customers_devices_amount', 'packages_devices.device_amount,customers_devices_amount.device_id', 'packages_devices', $join_condition, "inner", $where_clause);
        $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as mid", "mid");

        foreach ($device_amt->result_array() as $r) {

            $device_name = $this->user_model->get_field('devices', 'device_name', array('mg_id' => $mg_id, 'device_id' => $r['device_id']));
            $device_stb = $this->user_model->get_field('devices', 'stb_model', array('mg_id' => $mg_id, 'device_id' => $r['device_id']));
            $is_mobile = $this->user_model->get_field('devices', 'mobile_model', array('mg_id' => $mg_id, 'device_id' => $r['device_id']));
//             var_dump($device_amt->result_array());
            if ($device_stb != 1) {
                $no_of_log = $this->user_model->get_field('customers_devices', 'no_of_login', array('cus_id' => $cus_id, 'device_id' => $r['device_id']));
                $free_no_of_log = $this->user_model->get_field('customers_devices', 'free_no_of_login', array('cus_id' => $cus_id, 'device_id' => $r['device_id']));
                $no_of_pack_dev = $this->user_model->get_pack_no_dev_count($cus_package_id, $sp_id, $r['device_id']);
                $device_free = ($no_of_log - $free_no_of_log) - $no_of_pack_dev;
                $tot_payment_amount += $r['device_amount'] * $device_free;
                $pymntDtlsArray[] = array(
                    'description' => $device_name . " - Lyca TV Application Access",
                    'unique_id' => $r['device_id'],
                    'amount' => $r['device_amount'],
                    'qty' => $device_free,
                    'type' => 'device',
                    'is_mobile' => $is_mobile
                );
            }
//            $this->gm->payment_details($payment_circle_id, $r['device_name'], $r['device_amount'], $r['device_id'], 3, 1);
        }
//        exit();
        $chn_amt = $this->user_model->get_channel_amt_renew_new($cus_id, $sp_id, $cus_package_id);
        foreach ($chn_amt->result_array() as $r) {
            $tot_payment_amount += $r['channel_amount'];
            $pymntDtlsArray[] = array(
                'description' => $r['display_name'],
                'unique_id' => $r['channel_id'],
                'amount' => $r['channel_amount'],
                'qty' => 1,
                'type' => 'channel'
            );
//            $this->gm->payment_details($payment_circle_id, $r['display_name'], $r['channel_amount'], $r['channel_id'], 4, 1);
        }

        //Additional channel sub package payment
        $chn_pack_amt = $this->user_model->get_additional_channel_sub_renew($cus_id, $cus_package_id, $sp_id);
        foreach ($chn_pack_amt as $r) {
            $tot_payment_amount += $r['amount'];
            $pymntDtlsArray[] = array(
                'description' => $r['sub_package_name'],
                'unique_id' => $r['id'],
                'amount' => $r['amount'],
                'qty' => 1,
                'type' => 'channel_pack'
            );
//            $this->gm->payment_details($payment_circle_id, $r['display_name'], $r['channel_amount'], $r['channel_id'], 4, 1);
        }
        //End additional channel sub package payment
//        $taxes = $this->gm->get_data('cus_packages_taxes', '*', array('customer_package_id' => $cus_package_id, "subscription_period_id" => $sp_id));
//        foreach ($taxes->result_array() as $r) {
//            $tax_type = $this->gm->get_tax($r['tax_id'], "tax_type");
//            $tax_per = $this->gm->get_country_tax($country_id, $r['tax_id']);
//            $tax_amt = ($tot_payment_amount * ($tax_per / 100));
//            $this->gm->payment_details($payment_circle_id, $tax_type . " " . $tax_per . "%", $tax_amt, $r['tax_id'], 5);
//            $tot_payment_amount += $tax_amt;
//        }
        $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as m_id", "m_id");
        $pckg_taxes = $this->user_model->get_tax_details($cus_package_id, $country_id, $mg_id);

//        foreach ($pckg_taxes->result_array() as $pt) {
//            if ($pt['tax_method']) {
//                $taxAmount = $tot_payment_amount * ($pt['tax_percentage'] / 100);
//                $tot_payment_amount += $taxAmount;
//            }
//        }
        // discount
        $discount_type = $this->input->post('discount_type');
        $discount_value = $this->input->post('precentage');
        $discount = '0.00';

        if (($discount_type != "") && ($discount_value != "")) {
            if ($discount_type == 0) {
                //amount
                $discount = number_format($discount_value, 2);
            } else {
                // precentage
                $discount = number_format((($tot_payment_amount * $discount_value) / 100), 2);
            }
        }

        $tot_payment_amount -= $discount;

        $taxArray = array();
        foreach ($pckg_taxes->result_array() as $res) {
            if ($res['tax_method']) {
                $tax = $tot_payment_amount * ($res['tax_percentage'] / 100);
                $tot_payment_amount += $tax;
                $taxArray[] = array(
                    'description' => $res['tax_type'] . " " . $res['tax_percentage'] . "% (Exclusive)",
                    'amount' => $tax,
                    'percentage' => $res['tax_percentage'],
                    'method' => $res['tax_method'],
                );
//                $this->gm->payment_details($payment_circle_id, $res['tax_type'] . " " . $res['tax_percentage'] . "%", $tax, $res['tax_id'], 5);
            } else {
                $tax = round((($tot_payment_amount * $res['tax_percentage']) / (100 + $res['tax_percentage'])), 2);
                $taxArray[] = array(
                    'description' => $res['tax_type'] . " " . $res['tax_percentage'] . "% (Inclusive)",
                    'amount' => $tax,
                    'percentage' => $res['tax_percentage'],
                    'method' => $res['tax_method'],
                );
//                $this->gm->payment_details($payment_circle_id, $res['tax_type'] . " " . $res['tax_percentage'] . "%", $tax, $res['tax_id'], 5);
            }
        }

                
        $is_paied = $this->process_payments($cus_id, $payment_circle_id, $tot_payment_amount, $payment_type, $postArray);
        
        if ($is_paied['err_type']) {
            $jsn_arry = array(
                'err_type' => 1,
                'msg' => 'Purchase successfully done. Thank You!',
                'rdrctTo' => site_url('customer-package/' . $cus_id)
            );
            $payment_circle_id = $is_paied['payment_circle_id'];
            if ($is_paied['dirct_or_card']) {
                $crd_dtil_id = $is_paied['drct_or_dtail_id'];
                $d_id = 0;
            } else {
                $d_id = $is_paied['drct_or_dtail_id'];
                $crd_dtil_id = 0;
            }
            $start_date = $this->user_model->todaydate();

            
            //@@@@@@@@@@@@@@@@@@@added for referral bonus redeem

            $referral_redeem_status = $this->input->post("referral_redeem");
            $referral_free_days = 0;

            if ($referral_redeem_status == 1) {

                //added for referral bonus
                $referral_free_days = $this->input->post("referral_free_days");

                $note = "Referral bonus - " . $referral_free_days . " Days added to free view";
                $log_note = " has added " . $referral_free_days . " Days to free view from refferal bonus";

                $updated_time = date("Y-m-d H:i:s");
                $log_date = date("Y-m-d");


                $referralPaymentDetails = array(
                    'cus_id' => $cus_id,
                    'amount_type' => 2,
                    'amount' => $referral_free_days,
                    'description' => $note,
                    'added_date' => $updated_time
                );

                $this->user_model->insert_data('referral_payment_details', $referralPaymentDetails);

                $referralLog = array(
                    'cus_id' => $cus_id,
                    'description' => $log_note,
                    'log_date' => $log_date,
                    'log_type' => 19,
                    'username' => $this->user_model->is_logged(),
                    'unique_id' => $cus_id
                );

                $this->user_model->insert_data('customers_log', $referralLog);

                $customers_end_date = $this->user_model->get_field('customer_package_details', 'end_date', array("cus_id" => $cus_id));
                $updated_end_date = date('Y-m-d', strtotime($customers_end_date . '+' . $referral_free_days . ' days'));

                // echo $updated_end_date; die();
                //  $this->gm->update_data('customer_package_details', array('end_date'=>$updated_end_date),array('cus_id'=>$cus_id));

                $this->user_model->update_data(refferals, array('states' => 100), array('cus_id' => $cus_id, 'states' => 1));

                $reason = "Free days from referral";
                $today_date = date("Y-m-d");

                $freeViewDetails = array(
                    'cus_id' => $cus_id,
                    'no_of_days' => $referral_free_days,
                    'reason' => $reason,
                    'new_date' => 19,
                    'new_date' => $updated_end_date,
                    'date' => $today_date,
                    'status' => 2
                );

                $this->user_model->insert_data('free_views', $freeViewDetails);
            }
            

            //@@@@@@@@@@@@@@@@@@@@@@@@                 


            $end_date = $this->dateoperations->sum($start_date, 'day', ($no_of_days + $no_of_rest_days + $no_of_free_days + $referral_free_days));
//            echo 'number of days'. $no_of_days;
//            echo 'number of rest days'. $no_of_rest_days;
//            echo 'number of free days'. $no_of_free_days;

            $data = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'cus_package_status' => 1
            );
            
//            var_dump($end_date);
//            exit();
            $this->user_model->update_data("customer_package_details", $data, array('cus_id' => $cus_id));
            /*
             * @ DEsc: Update erosnow package end date to new renewed end date.
             */
            
            $eros_details = $this->user_model->get_data("customer_eros_package_details", "subscription_id", array('cus_id' => $cus_id))->row();

            if($eros_details->subscription_id==10){
                
            $this->db->where(array('cus_id' => $cus_id));
    
            $this->db->update('customer_eros_package_details', array('end_date' => $end_date));
            
            }

            // tentkotta renew
            
            $tent_details = $this->user_model->get_data("customer_tentkotta_package_details", "start_date,end_date,refID,cus_id", array('cus_id' => $cus_id,'subscription_id' => 10))->row();



            if ($tent_details->cus_id && $sp_id==10) {


                $this->load->model('tentkotta_model');
                $this->tentkotta = new tentkotta_model;
                $email = "lycaadmin@lyca.com";
                $password = "lyca";
                $result = $this->tentkotta->tentkotta_get_token($email, $password);
                $access_token = $result['token'];


                $result = $this->tentkotta->tentkotta_custom_renew($tent_details->start_date, $end_date, $access_token, $tent_details->refID);


                if ($result[status] != "") {
                    $this->db->where(array('cus_id' => $cus_id));
                    $this->db->update('customer_tentkotta_package_details', array('end_date' => $end_date));
                }
            }


//            $data = array(
//                'customer_status' => 1
//            );
//            $this->gm->update_data("customers", $data, array('cus_id' => $cus_id));

            if ($postArray['payment_method'] == 121 || $postArray['payment_method'] == 122 || $postArray['payment_method'] == 123) {
                $tot_payment_amount = 0;
                $discount = 0;
            }

            $circleArray = array(
                'cus_id' => $cus_id,
                'paid_amount' => $tot_payment_amount,
                'dis_amount' => $discount,
                'tax_details' => serialize($taxArray),
                'payment_method' => $is_paied['payment_method_type'],
                'payment_type' => $payment_type,
                'insert_time' => date('Y-m-d H:i:s'),
                'app_date' =>$this->user_model->todaydate(),
                'payment_detail_id' => $crd_dtil_id,
                'd_id' => $d_id,
                'paid_time' => date('Y-m-d H:i:s'),
                'paid' => 1,
                'pymntDtlsArray' => serialize($pymntDtlsArray),
                'bank_code' => $bank_code
            );
            
            
            $this->user_model->payment_circle_log($circleArray, $payment_circle_id, 0);
            


//            $this->gm->payment_circle_log($cus_id, $payment_type, $tot_payment_amount, $crd_dtil_id, $d_id, 1, 0, $payment_circle_id, $is_paied['transaction_id']);
            if ($is_paied['payment_method_type'] == 27 || $is_paied['payment_method_type'] == 28) {

                if ($is_paied['payment_method_type'] == 27) {
                    $phone_no = $is_paied['phone_no'];
                } else {
                    $phone_no = 0;
                }
                $this->user_model->insert_customer_transaction_table($cus_id, $payment_circle_id, $is_paied['payment_method_type'], $is_paied['responce_arr'], $phone_no);
            }
            
            //$this->gm->set_cus_status($cus_id);
//            if ($opt == 0) {
//                $cus_status = 1;
//            } else {
//                $cus_status = 3;
//            }
//            $data = array(
//                'customer_status' => $cus_status
//            );
//            $this->gm->update_data("customers", $data, array('cus_id' => $cus_id));


   
            if ($customer_status != 2) {
                $this->user_model->set_cus_status($cus_id);
                
            } else {
                $opt = $this->user_model->check_cus_stb($cus_id);

                if ($opt == 0) {
                    $cus_status = 1;                                

                } else {
                    $cus_status = 3;
                }
                
                $data = array(
                    'customer_status' => $cus_status
                );
                
                
              //  $this->user_model->update_data("customers", $data, array('cus_id' => $cus_id));

                $this->user_model->update_cus_last_activated_history($cus_id, 'renew');
                                                echo "here";exit();

            }


            
            $this->user_model->customers_log($cus_id, 101, $this->user_model->timenow(), " has renewed his package", $payment_circle_id);
           $this->user_model->tentkotta_api_log($cus_id, 101, $this->user_model->timenow(), " has renewed his package", $payment_circle_id);

            
//            $this->gm->package_renew($cus_id, $crd_dtil_id, 0, $payment_circle_id);
        
            
            $this->user_model->payment_log($payment_circle_id, $crd_dtil_id, 1);            
                      
            $data2 = array('cus_id' => $cus_id, 'payment_circle_id' => $payment_circle_id, 'attach' => $payment_circle_id);
                     
            
          //  $this->user_model->_send_email("additional_devices_other", $cus_email, $data2, " Package has renewed successfully.");           

            
            $this->response([
                           'status' => REST_Controller::HTTP_CREATED,
                           'message' => 'Package renewed successfully',
                              ], REST_Controller::HTTP_CREATED); // BAD_REQUEST (400) being the HTTP response code
            
        } else {
//echo json_encode($jsn_arry);
//exit();
            // $this->gm->payment_circle_log($cus_id, $payment_type, $tot_payment_amount, $crd_dtil_id, $d_id, 0, 0, $payment_circle_id, $is_paied['transaction_id']);
            $this->user_model->customers_log($cus_id, 15, timenow(), " Payment got faild", $payment_circle_id);
             $this->user_model->tentkotta_api_log($cus_id, 15, timenow(), " Payment got faild", $payment_circle_id);

            $jsn_arry = array(
                'err_type' => 0,
                'msg' => $is_paied['msg'],
//                            'rdrctTo' => site_url("customer-devices/" . $cus_id)
            );
//                        $this->session->set_userdata('err_type', 'warning');
//                        $this->session->set_userdata('err_msg', $is_paied['msg']);
                    $this->response([
                           'status' => REST_Controller::HTTP_BAD_REQUEST,
                           'message' => 'Failed to Renew package',
                              ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }
             }
        
         }
        
        
        
    
        else {       
                   $this->response([
                           'status' => REST_Controller::HTTP_BAD_REQUEST,
                           'message' => 'Invalid parameters',
                              ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
           }

       }

    
    
    
     function process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray, $auto_renew = 0) {
        $query = "SELECT currency_code, account_no, cus_title, first_name, created_by, last_name, country_name, lyca_cus, mobile_no, telephone_no, "
                . "fax_no, email, timezone, customer_status, registered_date, promo_type, start_date, end_date, no_of_days, telephone_code "
                . "shrt_frm_cus, Y.* FROM customers C JOIN countries N ON C.country_id = N.country_id "
                . "JOIN payment_circle Y ON (C.cus_id = Y.cus_id AND Y.payment_circle_id = $payment_circle_id) "
                . "LEFT JOIN customer_package_details D ON C.cus_id = D.cus_id";
        
        $cus_details = $this->db->query($query)->row();
        
       // print_r($cus_details); exit();
        
        $cus_status = $cus_details->customer_status;
        if ($cus_status == 1 || $cus_status == 3 || $cus_status == 2 || $cus_status == 0) {
            
            $username = $this->user_model->is_logged();
            if($username==""){
                $username="Cinematix LLC";
            }
            
            $phone_no = $cus_details->mobile_no;
            
            
            
            
          //  $country_code = str_replace('+', "", $cus_details->telephone_code);
            $country_code = "GB";

            
            $account_no = $cus_details->account_no;
            $c_code = $country_code;
            $note = '';
            $mob_no_zero = ltrim($phone_no, '0');
            $phone_no = $country_code . $mob_no_zero;


            // enter email address
            $cus_email = $this->user_model->get_customer($cus_id, "email");


            $payment_method_id = $postArray['payment_method'];

            
            if ($payment_method_id == 121 || $payment_method_id == 122 || $payment_method_id == 124) {
                $payment_method_type = 0;
            } else {
                $payment_method_type = $this->user_model->get_payment_method($payment_method_id, "type");
            }

            if ($payment_method_type == 0) {

                $data = array(
                    'cus_id' => $cus_id,
                    'note' => $note,
                    'username' => $username,
                    'payment_method_id' => $payment_method_id
                );
                $this->db->insert("direct_payments", $data);
                $d_id = $this->db->insert_id();

       
                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $d_id,
                        'dirct_or_card' => 0,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment Successfull. Thank You!'
                    );
                    return $jsn_arry;                
            } 
            else {               
                
                $card_type = $postArray['card_type_d'];
                $card_no_1 = $postArray['card_no_1'];
                $card_no_2 = $postArray['card_no_2'];
                $card_no_3 = $postArray['card_no_3'];
                $card_no_4 = $postArray['card_no_4'];
                $card_no = $card_no_1 . $card_no_2 . $card_no_3 . $card_no_4;
                $exp_month = $postArray['exp_month'];
                if ($exp_month < 10) {
                    $exp_month = '0' . $exp_month;
                }
                $exp_year = $postArray['exp_year'];

                $name_on_card = $postArray['name_on_card'];
                $cvc_no = $postArray['cvc_no'];

                if ($postArray['auto_renew'] == 1 AND $paid_amount != 0) {
                    // @@ Get Payment and save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount == 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->wirecard_auth_check($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount != 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } else {
                    // @@ Normal payment
                    $auth_code = $this->gm->wirecard_2($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id);
                }
                if ($auth_code == "0") {
                    $jsn_arry = array(
                        'err_type' => 0,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => '',
                        'dirct_or_card' => 1,
                        'msg' => 'Ooops!!! We could not process your payment.Please contact the bank for more details.Your Account no is ' . $account_no
                    );
                    $log_type = 3;
                    $this->user_model->customers_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);
                    $this->user_model->tentkotta_api_log($cus_id, $log_type, timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);

                    return $jsn_arry;
                } else {                  
                    
              //// End of customer_recurring_subscription_details
                    $card_data = array(
                        'cus_id' => $cus_id,
                        'payment_method_id' => $payment_method_id,
                        'card_type' => $card_type,
                        'card_no' => $card_no_1 . " " . $card_no_2 . " " . $card_no_3 . " " . $card_no_4,
                        'name_on_card' => $name_on_card,
                        'cvc_no' => $cvc_no,
                        'expired_from' => $exp_month . "/" . $exp_year,
                        'note' => $note,
                        'primary' => '',
                        'username' => $this->user_model->is_logged(),
                        'last_update' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("customers_payment_details", $card_data);
                    $payment_detail_id = $this->db->insert_id();

                    $data = array(
                        'paid_time' => date('Y-m-d H:i:s'),
                        'paid' => 1,
                        'payment_detail_id' => $payment_detail_id,
                        'auth_code' => $auth_code
                    );
                    $this->user_model->update_data("payment_circle", $data, array('payment_circle_id' => $payment_circle_id));

                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $payment_detail_id,
                        'dirct_or_card' => 1,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment successfull. Thank You!'
                    );
                    
                    
                    return $jsn_arry;
                                      
                }
            }
        } else {
            $jsn_arry = array(
                'err_type' => 0,
                'payment_circle_id' => '',
                'drct_or_dtail_id' => '',
                'dirct_or_card' => '',
                'msg' => 'Customer has not authorized to buy device'
            );
            return $jsn_arry;
        }
    }
}
