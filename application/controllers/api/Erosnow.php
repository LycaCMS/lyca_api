<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Erosnow extends REST_Controller {

    protected $methods = [
          'box_activation_post' => ['level' => 10],
        ];




       public function erosnow_post() {

        ob_start();
        $user_id = $this->post("userId");
        $erosnow_subscription = $this->post("subscription");



        if (!$user_id == "" && !$erosnow_subscription == "" && !$erosnow_subscription == 0 && !(preg_match('/[^a-z_\-0-9]/i', $user_id))) {

            if($erosnow_subscription!=7 && $erosnow_subscription!=8 && $erosnow_subscription!=9 && $erosnow_subscription!=10 && $erosnow_subscription!=11 && $erosnow_subscription!=12 && $erosnow_subscription!=13){
                  $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'Invalid Erosnow subscription periods',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
            
            }

            if ($this->user_model->check_data("payment_circle", array('payment_circle_id' => $user_id)) < 1) {

                $this->response([
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message' => 'This Customer Do not have Registered Account',
                        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code    
            } else {
                $cus_id = $this->user_model->get_data('payment_circle', '*', array('payment_circle_id' => $user_id))->row()->cus_id;
                $check_cus_status = $this->user_model->get_data('customers', '*', array('cus_id' => $cus_id))->row()->customer_status;
                if (($check_cus_status != 1) && ($check_cus_status != 3)) {
                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Customer is not allowed for add Erosnow'
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }


                  $cus_end_date = $this->user_model->get_customer($cus_id, "end_date");
                    $today = $this->user_model->todaydate();              


                     if($cus_end_date<$today){

                         $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,                     
                            'message' => 'Customer Subscription has Expired'             
                           ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    }

                $stb_device = $this->user_model->check_data("customers_devices", array('cus_id' => $cus_id, 'device_id' => 19));
                //  echo $stb_device;exit();
                if ($stb_device < 1) {

                    $this->response([
                        'status' => REST_Controller::HTTP_BAD_REQUEST,
                        'message' => 'Customer should have a Lyca STB to activate this package'
                            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }

                // $paid_amount = $this->user_model->get_payment_circle($payment_circle_id, "paid_amount");
                

                 $payment_method_id = 124;
                 
                                 
                 
                 
                 $subscription_period_id = $erosnow_subscription;
                 $language_id = $this->user_model->get_customer($cus_id, "language_id");
                 $country_id = $this->user_model->get_customer($cus_id, "customers.country_id as con_id", "con_id");
               //  $erosnow_details = $this->user_model->get_data('erosnow_subscription', '*', array('sbscrptn_id' => $subscription_period_id, 'lang_id' => $language_id, 'country_id' => $country_id, 'status' => 1))->row();

 $erosnow_details = $this->user_model->get_data_join('erosnow_subscription', '*', 'subscription_periods', 'erosnow_subscription.sbscrptn_id = subscription_periods.subscription_period_id', 'INNER',  array('sbscrptn_id' => $subscription_period_id, 'lang_id' => $language_id, 'country_id' => $country_id, 'status' => 1))->row();
      

                 
                if ($erosnow_details->eros_subscrptn_id != 0 && $new_customer_package_id != 575) {
                 
                $paid_amount=$erosnow_details->amount; 
       
                $payment_type = 3;
                $postArray['payment_method']=30;


                $circleArray = array(
                    'cus_id' => $cus_id,
                    'paid_amount' => $paid_amount,
                    'dis_amount' => 0.00,
                    'tax_details' => '',
                    'payment_method' => $postArray['payment_method'],
                    'payment_type' => '',
                    'insert_time' => date('Y-m-d H:i:s'),
                    'app_date' => date('Y-m-d'),
                    'payment_detail_id' => '',
                    'd_id' => '',
                    'paid_time' => '',
                    'paid' => 0,
                    'pymntDtlsArray' => ''
                );


                $payment_circle_id = $this->user_model->payment_circle_log($circleArray, '', 1);


                $is_paied = $this->process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray);
                 
                          
                
                  $is_paied['err_type']=1;
        
                    if ($is_paied['err_type']) {

                         if ($is_paied['dirct_or_card']) {
                            $crd_dtil_id = $is_paied['drct_or_dtail_id'];
                            $d_id = 0;
                        } else {
                            $d_id = $is_paied['drct_or_dtail_id'];
                            $crd_dtil_id = 0;
                        } 

                

              
                    $new_customer_package_id = $this->user_model->get_customer($cus_id, "customer_package_details.customer_package_id as cus_pack_id", "cus_pack_id");
                    $new_subscription_period_id = $this->user_model->get_customer($cus_id, "subscription_period_id");
                    $cus_result = $this->user_model->get_data('customer_package_details', 'end_date', array('cus_id' => $cus_id))->row();

                    $subscription_day = $this->user_model->get_data('subscription_periods', 'no_of_days', array('subscription_period_id' => $subscription_period_id,'mg_id' => 2))->row()->no_of_days;
                    $cus_day = $this->user_model->get_data('customer_package_details', 'no_of_days', array('cus_id' => $cus_id))->row()->no_of_days;

                   // var_dump($cus_day.'>='.$subscription_day);exit;


                    $today = $this->user_model->todaydate();


                    if($cus_day<$subscription_day){
                        
                        $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,
                            'message' => "Customer remaining days should be larger than the ErosNow Subscription days",
                                ], REST_Controller::HTTP_BAD_REQUEST); // CREATED (201) being the HTTP response code  
                        
                    }

                    $cus_end_date = $this->user_model->get_customer($cus_id, "end_date");


                   // echo "here"; exit();

                    $end = strtotime($today);
                    $start = strtotime($cus_end_date);
                    $expire_time = round(($start - $end) / (60 * 60 * 24), 0);
                    $pack_subs_days = $this->user_model->get_field('subscription_periods', 'no_of_days', array('subscription_period_id' => $subscription_period_id, 'mg_id' => 2));



                    if (($pack_subs_days > $expire_time) && !($subscription_period_id == 10)) {
                    
                    
                    if($expire_time < $pack_subs_days){
                        //$paid_amount=$erosnow_details->amount;
                            $erosnow_no_of_days = $erosnow_details->no_of_days;
                        if ($paid_amount > 0) {
                            $per_day_amount = ($paid_amount / $erosnow_no_of_days);
                            $paid_amount = round($per_day_amount * $expire_time, 2);
                        }
                    }else{
                        $this->response([
                            'status' => REST_Controller::HTTP_BAD_REQUEST,
                            'message' => "Customer remaining days should be larger than the ErosNow Subscription days",
                                ], REST_Controller::HTTP_BAD_REQUEST); // CREATED (201) being the HTTP response code  
                        
                    }

                    }




                    if ($subscription_period_id == 10) {
                        $erosnow_end_date = $cus_end_date;
                    } else {

                       if($expire_time > $pack_subs_days){

                               $erosnow_end_date = $this->dateoperations->sum($today, 'day', ($pack_subs_days));

                         }  else {
                               $erosnow_end_date = $cus_end_date;

                         }
                    }


                    //   $erosnow_details = $this->user_model->get_data('erosnow_subscription', '*', array('sbscrptn_id' => $subscription_period_id, 'language_id' => $language_id, 'country_id' => $country_id, 'status' => 1))->row();

     

                        $cus_details = $this->user_model->get_data_join('customers', 'mobile_no, email, first_name, account_no,last_name,postal_code, telephone_code, cus_password, country_code', 'countries', 'customers.country_id = countries.country_id', 'INNER', array('cus_id' => $cus_id))->row();

                        $erosnow_start_date = date('Y-m-d');
                        //$paid_amount = $erosnow_details->amount;

                        $country = $this->user_model->get_data_join('countries', 'country_code', 'customers', 'customers.country_id=countries.country_id', 'INNER', array('cus_id' => $cus_id))->row();
                        $cus_email = $cus_details->email;
                        //$paid_amount = $erosnow_details->amount;

                        
                       
                     
                        
                // $this->load->model('eros_now_model',TRUE);
                //$this->eros_now = new eros_now_model;  
                                
                      
                    

                  $this->load->model('eros_now_model');  
                         
                  $this->eros_now = new eros_now_model();                      
                           
                        
                       
                        //$email = "lycaadmin@lyca.com";
                        //$password = "lyca";
                        // $result['errorCode']="";
                        // $result1 = $this->eros_now->login_to_erosnow($user_id);
                        //  var_dump($result1);
                        //  exit();
                        // $access_token = $result1['token']; 

                        $result = $this->eros_now->erosnow_user_register($cus_details->mobile_no, $cus_details->telephone_code, $user_id, $cus_details->cus_password . 'lyca', $country->country_code);
                         ob_end_clean();               
                         if($result==0){
                           $this->response([
                                'status' => REST_Controller::HTTP_BAD_REQUEST,
                                'message' => "Already Added",
                                    ],REST_Controller::HTTP_BAD_REQUEST);exit;
                          }


                        $result_purchase = $this->eros_now->erosnow_purchase($user_id, $result['token'], $result['token_secret']);
                        
                        
                       // $paymntcrlc_id = $this->user_model->payment_circle_log($circleArray, '', 1);
                        $insert_data = array(
                            'cus_id' => $cus_id,
                            'register_response' => serialize($result),
                            'purchase_response' => serialize($result_purchase)
                        );
                        
                        $this->db->insert('eros_subscription_response_data', $insert_data);
                        
                            $pymntDtlsArray = array();
                        $stbModls = 0;  
                        if ($subscription_period_id == 10) {

                            $pymntDtlsArray[] = array(
                                'description' => "eros now Package Free subscription",
                                'amount' => 0,
                                'qty' => 1,
                                'type' => 'eros now'
                            );
                        } else {
                            $pymntDtlsArray[] = array(
                                'description' => "eros now Package subscription",
                                'amount' => $paid_amount,
                                'qty' => 1,
                                'type' => 'eros now'
                            );
                        }

                     
                      
                        $circleArray = array(
                            'cus_id' => $cus_id,
                            'paid_amount' => $paid_amount,
                            'dis_amount' => 0,
                            'discount_details' => '',
                            'tax_details' => '',
                            'payment_method' => $is_paied['payment_method_type'],
                            'payment_type' => 16,
                            'insert_time' => date('Y-m-d H:i:s'),
                            'app_date' => $this->user_model->todaydate(),
                            'payment_detail_id' => $crd_dtil_id,
                            'd_id' => $d_id,
                            'paid_time' => date("Y-m-d H:i:s"),
                            'paid' => 1,
                            'pymntDtlsArray' => serialize($pymntDtlsArray),
                            'pending_stb_cnt' => 0,
                            'additionalChargesArray' => ''
                        );

                          $paymntcrlc_id = $this->user_model->payment_circle_log($circleArray, '', 1);
                        

                        if ($this->user_model->check_data("customer_eros_package_details", array('cus_id' => $cus_id)) < 1) {
                       


                            $insert_array = array(
                                'start_date' => $erosnow_start_date,
                                'end_date' => $erosnow_end_date,
                                'status' => 1,
                                'payment_circle_id' => $paymntcrlc_id,
                               // 'user_id' => $result['userId'],
                                'registerd_password' => $cus_details->cus_password,
                                'registerd_mobile_number' => $cus_details->mobile_no,
                                'subscription_id' => $subscription_period_id,
                                'cus_id' => $cus_id,
                                'eros_pack_id' => $erosnow_details->eros_subscrptn_id,
                                'purchased_amount' => $paid_amount
                            );


                            $this->user_model->insert_data("customer_eros_package_details", $insert_array, array('cus_id' => $cus_id));


                            // $data_erosnow = array('cus_id' => $cus_id, 'payment_circle_id' => $paymntcrlc_id,'tentkotta_start_date' => $erosnow_start_date,'tentkotta_password' => $result['password']);

                            $this->user_model->customers_log($cus_id, 25, $this->user_model->timenow(), "has registered for Erosnow Package", $paymntcrlc_id);
							 $this->user_model->tentkotta_api_log($cus_id, 25, $this->user_model->timenow(), "has registered for Erosnow Package", $paymntcrlc_id);	
                            

                            $this->response([
                                'status' => REST_Controller::HTTP_CREATED,
                                'message' => "Erosnow Package Suceesfully Added",
                                    ],REST_Controller::HTTP_CREATED);exit;  

                         
                        } else {

                            $this->response([
                                'status' => $result['errorCode'],
                                'message' => $result['message'],
                                    ],REST_Controller::HTTP_BAD_REQUEST); // CREATED (201) being the HTTP response code  
                        }
                    } 
                }
                
                else {
                    
                    
                   $this->response([                    
                    'status' => REST_Controller::HTTP_BAD_REQUEST,
                    'message'=>"No Eros Packages Available For This Subscription",
                ],REST_Controller::HTTP_BAD_REQUEST); // CREATED (201) being the HTTP response code  

                
                    
                }
                
            }
        } else {
            $this->response([
                'status' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid parameters',
                    ],REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
          
    }

          function process_payments($cus_id, $payment_circle_id, $paid_amount, $payment_type, $postArray, $auto_renew = 0) {

        $query = "SELECT currency_code, account_no, cus_title, first_name, created_by, last_name, country_name, lyca_cus, mobile_no, telephone_no, "
                . "fax_no, email, timezone, customer_status, registered_date, promo_type, start_date, end_date, no_of_days, telephone_code "
                . "shrt_frm_cus, Y.* FROM customers C JOIN countries N ON C.country_id = N.country_id "
                . "JOIN payment_circle Y ON (C.cus_id = Y.cus_id AND Y.payment_circle_id = $payment_circle_id) "
                . "LEFT JOIN customer_package_details D ON C.cus_id = D.cus_id";
        
        $cus_details = $this->db->query($query)->row();
        
       // print_r($cus_details); exit();
        
        $cus_status = $cus_details->customer_status;
        if ($cus_status == 1 || $cus_status == 3 || $cus_status == 2 || $cus_status == 0) {
            
            $username = $this->user_model->is_logged();
            if($username==""){
                $username="Cinematix LLC";
            }
            
            $phone_no = $cus_details->mobile_no;
            
            
            
            
          //  $country_code = str_replace('+', "", $cus_details->telephone_code);
            $country_code = "GB";

            
            $account_no = $cus_details->account_no;
            $c_code = $country_code;
            $note = '';
            $mob_no_zero = ltrim($phone_no, '0');
            $phone_no = $country_code . $mob_no_zero;


            // enter email address
            $cus_email = $this->user_model->get_customer($cus_id, "email");


             $payment_method_id = $postArray['payment_method'];

            if ($payment_method_id == 121 || $payment_method_id == 122 || $payment_method_id == 124) {
                $payment_method_type = 0;
            } else {
                $payment_method_type = $this->user_model->get_payment_method($payment_method_id, "type");
            }

            if ($payment_method_type == 0) {

                $data = array(
                    'cus_id' => $cus_id,
                    'note' => $note,
                    'username' => $username,
                    'payment_method_id' => $payment_method_id
                );
                $this->db->insert("direct_payments", $data);
                $d_id = $this->db->insert_id();

       
                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $d_id,
                        'dirct_or_card' => 0,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment Successfull. Thank You!'
                    );
                    return $jsn_arry;                
            } 
            else {               
                
                $card_type = $postArray['card_type_d'];
                $card_no_1 = $postArray['card_no_1'];
                $card_no_2 = $postArray['card_no_2'];
                $card_no_3 = $postArray['card_no_3'];
                $card_no_4 = $postArray['card_no_4'];
                $card_no = $card_no_1 . $card_no_2 . $card_no_3 . $card_no_4;
                $exp_month = $postArray['exp_month'];
                if ($exp_month < 10) {
                    $exp_month = '0' . $exp_month;
                }
                $exp_year = $postArray['exp_year'];

                $name_on_card = $postArray['name_on_card'];
                $cvc_no = $postArray['cvc_no'];

                if ($postArray['auto_renew'] == 1 AND $paid_amount != 0) {
                    // @@ Get Payment and save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount == 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->wirecard_auth_check($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } elseif ($paid_amount != 0 AND $cus_details->promo_type != '') {
                    // @@ Don't get payment but save card details
                    $auth_code = $this->gm->get_payment_n_save_card($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id, $card_no_4, $payment_method_id, $card_type);
                } else {
                    // @@ Normal payment
                    $auth_code = $this->gm->wirecard_2($cus_id, $paid_amount, $card_no, $cvc_no, $exp_year, $exp_month, $payment_circle_id);
                }
                if ($auth_code == "0") {
                    $jsn_arry = array(
                        'err_type' => 0,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => '',
                        'dirct_or_card' => 1,
                        'msg' => 'Ooops!!! We could not process your payment.Please contact the bank for more details.Your Account no is ' . $account_no
                    );
                    $log_type = 3;
                    $this->user_model->customers_log($cus_id, $log_type, $this->user_model->timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);
                    $this->user_model->tentkotta_api_log($cus_id, $log_type, $this->user_model->timenow(), "<span class='text-danger'>Bank was declined the payment process</span>", $payment_circle_id);
                    return $jsn_arry;
                } else {                  
                    
              //// End of customer_recurring_subscription_details
                    $card_data = array(
                        'cus_id' => $cus_id,
                        'payment_method_id' => $payment_method_id,
                        'card_type' => $card_type,
                        'card_no' => $card_no_1 . " " . $card_no_2 . " " . $card_no_3 . " " . $card_no_4,
                        'name_on_card' => $name_on_card,
                        'cvc_no' => $cvc_no,
                        'expired_from' => $exp_month . "/" . $exp_year,
                        'note' => $note,
                        'primary' => '',
                        'username' => $this->user_model->is_logged(),
                        'last_update' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("customers_payment_details", $card_data);
                    $payment_detail_id = $this->db->insert_id();

                    $data = array(
                        'paid_time' => date('Y-m-d H:i:s'),
                        'paid' => 1,
                        'payment_detail_id' => $payment_detail_id,
                        'auth_code' => $auth_code
                    );
                    $this->user_model->update_data("payment_circle", $data, array('payment_circle_id' => $payment_circle_id));

                    $jsn_arry = array(
                        'err_type' => 1,
                        'payment_circle_id' => $payment_circle_id,
                        'drct_or_dtail_id' => $payment_detail_id,
                        'dirct_or_card' => 1,
                        'payment_method_type' => $payment_method_id,
                        'msg' => 'Payment successfull. Thank You!'
                    );
                    
                    
                    return $jsn_arry;
                                      
                }
            }
        } else {
            $jsn_arry = array(
                'err_type' => 0,
                'payment_circle_id' => '',
                'drct_or_dtail_id' => '',
                'dirct_or_card' => '',
                'msg' => 'Customer has not authorized to buy device'
            );
            return $jsn_arry;
        }
    }
}
