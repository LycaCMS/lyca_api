<?php

class tentkotta_model extends CI_Model {

    private $partner = 'LYCA';
    private $protocol = "https";
    //private $myenv = "stgapi.erosnow.com";
    private $myenv = "api.tentkotta.com/tkapi/v1/user/";

    function __construct() {
        parent::__construct();
        $this->load->library('tentkotta_lib');
    }

      
    
     function tentkotta_user_register($email,$tentkotta_pack_id,$first_name,$last_name,$mobile_no, $postal_code,$country_code,$access_token) {
        
        $post_url = $this->protocol . "://" . $this->myenv . "subscription/add";                
        $post_array = array("email" =>$email, "plan" =>$tentkotta_pack_id,"firstName" =>$first_name, "lastName" =>$last_name, 'phoneNumber' =>$mobile_no,'zipcode' =>$postal_code,'countryCode' =>$country_code,'access_token'=>$access_token);
        
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE);  
        
        return $result;
        
    }

    function tentkotta_cancel_subscription($email,$access_token) {
        $post_url = $this->protocol . "://" . $this->myenv . "subscription/cancel";
        $post_array = array("email" => $email,"access_token" => $access_token);
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }
    
     function tentkotta_forgot_password($email,$access_token) {
        $post_url = $this->protocol . "://" . $this->myenv . "lyca/forgotPwd";
        $post_array = array("email" => $email,"access_token" => $access_token);
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }
   
    function tentkotta_get_token($email,$password) {
        $post_url = $this->protocol . "://" . $this->myenv . "lyca/login";
        $post_array = array("username" => $email,"password" => $password);
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result; 
    }


    function tentkotta_custom_subscription($email,$mobile_no,$first_name,$last_name, $postal_code,$country_code,$tentkotta_start_date,$tentkotta_end_date,$access_token) {
        $post_url = $this->protocol . "://" . $this->myenv . "customSubscription/add";
        
        $post_array = array("email" =>$email,'phoneNumber' =>$mobile_no,"firstName" =>$first_name, "lastName" =>$last_name, 'zipcode' =>$postal_code,'countryCode' =>$country_code,'startDate' =>$tentkotta_start_date,'endDate' =>$tentkotta_end_date,'access_token'=>$access_token);

         
       $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE); 


    // print_r($result); eixt();
        
      return $result;
    }


    function tentkotta_custom_renew($tentkotta_start_date,$tentkotta_end_date,$access_token,$refID) {
       
        $post_url = $this->protocol . "://" . $this->myenv . "customSubscription/$refID";
        
        $post_array = array('startDate' =>$tentkotta_start_date,'endDate' =>$tentkotta_end_date,'access_token'=>$access_token);
         
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'PUT', TRUE); 
       
        return $result;
    }


   
    function tentkotta_custom_cancel($tentkotta_start_date,$tentkotta_end_date,$access_token,$refID) {       

        $post_url = $this->protocol . "://" . $this->myenv . "customSubscription/$refID";
        
        $post_array = array('startDate' =>$tentkotta_start_date,'endDate' =>$tentkotta_end_date,'access_token'=>$access_token);
         
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'PUT', TRUE); 
        
        return $result;
    }


    function tentkotta_activate_device($active_pin,$user_id,$access_token) {
        $post_url = $this->protocol . "://" . $this->myenv . "lyca/activateDevice";
        $post_array = array("userId" => $user_id,"pin" => $active_pin,"access_token" => $access_token);
        $result = $this->tentkotta_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result; 
    }

    

}
