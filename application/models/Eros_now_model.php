<?php
class eros_now_model extends CI_Model {

    private $partner = 'LYCA';
    private $protocol = "https";
    //private $myenv = "stgapi.erosnow.com";
    private $myenv = "api.erosnow.com";

    function __construct() {
        parent::__construct();
        $this->load->library('Erosnow_lib');
    }

    function login_to_erosnow($pyment_circle_id) {
        
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/login";
        $post_array = array("id" => $pyment_circle_id, "partner" => $this->partner, "type" => "force");
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }

    function erosnow_user_register($mobile, $tel_code, $payment_circle_id, $password, $country_code) {
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/register";
        $post_array = array("calling_code" => $tel_code, "mobile" => $mobile, "partner" => $this->partner, "partnerid" => $payment_circle_id, 'password' => sha1($password), 'country' => $country_code);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'POST', TRUE);        
        return $result;
    }

    function erosnow_purchase($payment_circle_id, $token, $token_secret) {
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/purchase";
        //@ Plan : 1000003 - Monthly Subscription USD -- US and ROW (7.99/mo)
        /////////: 1000125 - Monthly Non-Recurring Subscription USD and ROW (7.99/mo)
        //@ Product : 1000003 - Always use this ID as LYCA use only premium
        //@ payment_id : 1000002 - Always use this ID 1000002 As LYCA use only
        $post_array = array("plan" => '1000445', "product" => '1000445', 'payment_id' => '1000002', 'partner' => $this->partner, "partnerid" => $payment_circle_id);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }

    function erosnow_cancel_subscription($payment_circle_id, $token) {
      //  $this->erosnow_lib->setTokenAndSecret($token['token'], $token['token_secret']);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/cancelpurchase";
        $post_array = array("partner" => $this->partner, "plan" => '1000445', "product" => '1000445', "partnerid" => $payment_circle_id);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }

    function reset_password($payment_circle_id, $password, $token, $token_secret) {
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/setpassword";
        $post_array = array("partner" => $this->partner, "partner_id" => $payment_circle_id, "password" => $password);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'PUT', TRUE);
        return $result;
    }

    function verify_device_api($device_code, $token, $token_secret){
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/logincode";
        $post_array = array("action" => 'verify', "code" => $device_code);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'GET', TRUE);
        return $result;
    }
    
    function authorize_device_api($device_code, $token, $token_secret){
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/logincode";
        $post_array = array("action" => 'authorize', "code" => $device_code);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'GET', TRUE);
        return $result;
    }

    function reset_mobile_numer($payment_circle_id, $telephone_code, $new_number, $token, $token_secret) {
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/setmobile";
        $post_array = array("partner" => $this->partner, "partner_id" => $payment_circle_id, 'calling_code' => $telephone_code, 'mobile' => str_replace(' ', '', $new_number));
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'PUT', TRUE);
        return $result;
    }
//    private function curl_call($post_url, $fields_string) {
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $post_url);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $server_output = curl_exec($ch);
//        curl_close($ch);
//        if ($server_output === false) {
//            echo 'Curl error: ' . curl_error($ch);
//        } else {
//            return $server_output;
//        }
//    }

    function erosnow_purchase_validity($payment_circle_id, $token, $token_secret, $expr_date) {
        $this->erosnow_lib->setTokenAndSecret($token, $token_secret);
        $post_url = $this->protocol . "://" . $this->myenv . "/api/v2/secured/user/purchase";
        //@ Plan : 1000003 - Monthly Subscription USD -- US and ROW (7.99/mo)
        /////////: 1000125 - Monthly Non-Recurring Subscription USD and ROW (7.99/mo)
        //@ Product : 1000003 - Always use this ID as LYCA use only premium
        //@ payment_id : 1000002 - Always use this ID 1000002 As LYCA use only
        $post_array = array("plan" => '1000445', "product" => '1000445', 'payment_id' => '1000002', 'partner' => $this->partner, "partnerid" => $payment_circle_id, 'partnerExpiry' => $expr_date);
        $result = $this->erosnow_lib->sendData($post_url, $post_array, 'POST', TRUE);
        return $result;
    }

}
?>
