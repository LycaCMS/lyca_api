<?php

class User_model extends CI_Model {

    public $_permissions = array();

    function __construct() {

        parent::__construct();
    }

    function is_logged() {

        return $this->session->userdata('loggedname');
    }

    function insert_data($tbl_name, $data) {

        return $this->db->insert($tbl_name, $data);
    }

    function insert_data_id($tbl_name, $data) {

        $this->db->insert($tbl_name, $data);
        return $this->db->insert_id();
    }
    
    function local_date($timezone = 'UTC', $datestring = "%Y-%m-%d", $daylight_saving = FALSE) {
        $timestamp = time();
        $t = gmt_to_local($timestamp, $timezone, $daylight_saving);
        return mdate($datestring, $t);
    }

    function get_acc_no($agent_id, $country_id) {

        $acc_no = "";

        $country_code = $this->get_country($country_id, "country_code");

        $main_agent_id = $this->get_agent($agent_id, "main_agent_id");

        if ($main_agent_id <> 0) {

            $main_agent_id = $this->get_agent($agent_id, "main_agent_id");
        }

        $mg_id = $this->get_mg_id($agent_id);



        $res = $this->get_data("customers", "*", array("mg_id" => $mg_id, 'country_id' => $country_id), "cus_id DESC", 1);

        if ($res->num_rows() == 0) {

            $company_short_code = $this->get_master_agent($mg_id, "company_short_code");

            if ($company_short_code == "") {

                $company_short_code = "AMG";
            }

            $acc_no = $company_short_code . "-" . $country_code . "-";



            $acc_no_start = $this->get_country($country_id, "account_no_start_from");

            $acc_no_start = ($acc_no_start == 0) ? 100000000 : $acc_no_start;



            $acc_no = $acc_no . $acc_no_start;

            return $acc_no;
        } else {

            $acc_no = $res->row(0)->account_no;

            $acc_no = $this->user_model->next_acc_no($acc_no);

            return $acc_no;
        }
    }

    function get_country($country_id, $field) {

        $this->db->select($field);

        $this->db->where("country_id", $country_id);

        $res = $this->db->get("countries");

        return $res->row(0)->$field;
    }

    function get_agent($agent_id, $field) {

        $this->db->select($field);

        $this->db->where("agent_id", $agent_id);

        $res = $this->db->get("agents");
        if ($res->num_rows() == 0) {
            return "Agent Removed";
        } else {
            return $res->row(0)->$field;
        }
    }

    function get_mg_id($agent_id) {

        $mg_id = 0;

        if ($agent_id == 0) {

            $mg_id = 1;
        } else {

            $main_agent_id = $this->get_agent($agent_id, "main_agent_id");

            if ($main_agent_id == 0) {

                $mg_id = $this->get_agent($agent_id, "mg_id");
            } else {

                $mg_id = $this->get_agent($main_agent_id, "mg_id");
            }
        }

        return $mg_id;
    }

    function get_master_agent($mg_id, $field) {

        $this->db->select($field);

        $this->db->where("mg_id", $mg_id);

        $res = $this->db->get("master_agents");

        return $res->row(0)->$field;
    }

    function next_acc_no($acc_no) {
        $n = $this->get_numerics($acc_no);
        foreach ($n as $y) {
            if ($y >= 1000) {
                $t = $y;
                $t++;
                break;
            }
        }
        $new_acc_no = $this->replace_numerics($acc_no, $t);
        return $new_acc_no;
    }

    function get_data($tbl_name, $field, $param = NULL, $order = NULL, $limit = NULL, $group = NULL) {
        $this->db->select($field);
        if ($param != NULL)
            $this->db->where($param);
        if ($order != NULL)
            $this->db->order_by($order);
        if ($group != NULL)
            $this->db->group_by($group);
        if ($limit != NULL)
            $this->db->limit($limit);
        return $this->db->get($tbl_name);
    }

    function get_subscription_periods($subscription_period_id, $field) {

        $this->db->select($field);

        $this->db->where("subscription_period_id", $subscription_period_id);

        $res = $this->db->get("subscription_periods");

        return $res->row(0)->$field;
    }

    function get_package_subscription_details($cus_package_id, $subscription_periord_id, $field) {
        $this->db->select($field);
        $this->db->where("customer_package_id", $cus_package_id);
        $this->db->where("subscription_period_id", $subscription_periord_id);
        $res = $this->db->get("cus_packages_subscription_periods");
        if ($res->num_rows() == 0) {
            return 0;
        } else {
            return $res->row(0)->$field;
        }
    }

    function get_data_join($tbl_name, $field, $join_tbl, $join_condition, $join_type = "inner", $param = NULL, $order = NULL, $group = NULL) {

        $this->db->select($field);

        $this->db->join($join_tbl, $join_condition, $join_type);

        if ($param != NULL)
            $this->db->where($param);

        if ($order != NULL)
            $this->db->order_by($order);

        if ($group != NULL)
            $this->db->group_by($group);

        return $this->db->get($tbl_name);
    }

    function get_numerics($acc_no) {

        preg_match_all('/\d+/', $acc_no, $matches);

        return $matches[0];
    }

    function replace_numerics($acc_no, $r) {

        $c = preg_replace('/\d+/', $r, $acc_no);

        return $c;
    }

    function sha1_encrypt($algo, $data, $salt) {
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);

        return hash_final($context);
    }

    function random_password($length = 8) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*()_=+-?";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    function reset_random_password($length = 8) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%&()";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    function todaydate($datestring = "%Y-%m-%d") {
        $time = time();
        return mdate($datestring, $time);
    }

    function timenow($datestring = "%Y-%m-%d %H:%i:%s") {
        $time = time();
        return mdate($datestring, $time);
    }

    function payment_circle_log($data, $prv_pymncrlId = '', $updtOrinsrt) {

        if ($updtOrinsrt) {
            $this->db->insert("payment_circle", $data);
        } else {
            $this->db->where('payment_circle_id', $prv_pymncrlId);
            $this->db->update("payment_circle", $data);
        }
        return $this->db->insert_id();
    }

    function get_package($package_id, $field, $ambiguous = NULL) {

        $this->db->select($field);

        $this->db->join("countries", "countries.country_id = cus_packages.country_id");

        $this->db->join("language", "language.id = cus_packages.lang_id");

        //$this->db->join("charges", "charges.customer_package_id = cus_packages.customer_package_id");

        $this->db->where("cus_packages.customer_package_id", $package_id);

        $this->db->group_by('cus_packages.customer_package_id');

        $res = $this->db->get("cus_packages");

        if ($ambiguous == NULL)
            return $res->row(0)->$field;
        else
            return $res->row(0)->$ambiguous;
    }

    function get_tax_details($cus_package_id, $country_id, $mg_id) {
        $join_array = array(
            array(
                'join_tbl' => 'taxes',
                'join_condition' => 'taxes.tax_id = cus_packages_taxes.tax_id',
                'join_type' => 'LEFT',
            ),
            array(
                'join_tbl' => 'countries_taxes',
                'join_condition' => 'countries_taxes.tax_id = cus_packages_taxes.tax_id',
                'join_type' => 'LEFT',
            ),
        );

        $data = $this->user_model->get_data_join_array('cus_packages_taxes', '*', $join_array, array('customer_package_id' => $cus_package_id, "countries_taxes.country_id" => $country_id, "countries_taxes.mg_id" => $mg_id));
        return $data;
    }

    function get_data_join_array($from_tbl, $fields, $join_array, $where = NULL, $order = NULL, $group = NULL, $or = NULL) {

        $this->db->select($fields);
        $this->db->from($from_tbl);
        if (!empty($join_array)) {
            foreach ($join_array as $jary) {
                $this->db->join($jary['join_tbl'], $jary['join_condition'], $jary['join_type']);
            }
        }
        if ($where != NULL) {
            $this->db->where($where);
        }
        if ($order != NULL) {
            $this->db->order_by($order);
        }
        if ($group != NULL) {
            $this->db->group_by($group);
        }
        if ($or != NULL) {
            $this->db->where_in('device_status', $or);
        }
        return $this->db->get();
    }

    function customers_log($cus_id, $log_type, $log_date, $des = "", $unquie_id = 0) {

        $data = array(
            'cus_id' => $cus_id,
            'description' => $des,
            'log_date' => $log_date,
            'unique_id' => $unquie_id,
            'username' => "API - Cinematix",
            'last_update' => date('Y-m-d H:i:s'),
            'log_type' => $log_type
        );
        $this->db->insert("customers_log", $data);
    }

    function get_pack_device_ct($cus_package_id, $sp_id) {
        $this->db->where("customer_package_id", $cus_package_id);
        $this->db->where("subscription_period_id", $sp_id);
        $res = $this->db->get("cus_packages_devices");
        return $res->num_rows();
    }

    function other_charges_devices() {
        $this->db->join("devices", "devices.device_id = charges_devices.device_id");
        $this->db->where('status', 1);
        $this->db->group_by("devices.device_id");
        return $this->db->get("charges_devices");
    }

    function get_device_price($package_id, $sp_id, $device_id) {

        $this->db->where("device_id", $device_id);

        $this->db->where("customer_package_id", $package_id);

        $this->db->where("subscription_period_id", $sp_id);

        $res = $this->db->get("packages_devices");

        if ($res->num_rows() == 0) {

            return "";
        } else {

            return ($res->row(0)->device_amount);
        }
    }

    function get_field($tbl_name, $field, $where) {

        $this->db->where($where);
        $result = $this->db->get($tbl_name);

        if ($result->result()) {
            $value = $result->row(0)->$field;
            return $value;
        }
    }
    
    function get_device($device_id, $field) {

        $this->db->select($field);

        $this->db->where("device_id", $device_id);

        $res = $this->db->get("devices");

        return $res->row(0)->$field;
    }

    function update_data($tbl_name, $data, $param) {

        $this->db->where($param);

        $update = $this->db->update($tbl_name, $data);
        return $update;
    }
    function get_customer($cus_id, $field, $ambiguous = NULL) {
        $this->db->select($field);
        $this->db->join("customer_package_details", "customer_package_details.cus_id = customers.cus_id");
        $this->db->join("android_box_serial", "android_box_serial.cus_id = customers.cus_id", "left");
        //$this->db->join("download_schedules", "download_schedules.ds_id = customer_package_details.ds_id");
        $this->db->join("countries", "countries.country_id = customers.country_id");
        $this->db->join("agents", "agents.agent_id = customers.agent_id", "left");
        $this->db->join("cus_packages", "cus_packages.customer_package_id = customer_package_details.customer_package_id");
        //to select source
        $this->db->join("source", "source.source_id=customers.how_did_you_find_us", "left");
        $this->db->where("customers.cus_id", $cus_id);
        $res = $this->db->get("customers");
        if ($ambiguous == NULL)
//            return ((isset($res->row(0)->$field) && (!empty($res->row(0)->$field))) ? $res->row(0)->$field : "");
            return ((isset($res->row(0)->$field) && ($res->row(0)->$field != '')) ? $res->row(0)->$field : "");
        else
//            return ((isset($res->row(0)->$ambiguous) && (!empty($res->row(0)->$ambiguous))) ? $res->row(0)->$ambiguous : "");
            return ((isset($res->row(0)->$ambiguous) && ($res->row(0)->$ambiguous != '')) ? $res->row(0)->$ambiguous : "");
    }
    
    function check_data($tbl_name, $param = NULL) {
        if ($param != NULL)
            $this->db->where($param);
        $res = $this->db->get($tbl_name);
        return $res->num_rows();
    }

     function get_user_auth($username,$password, $field) {
        $this->db->select($field);
        $this->db->where("user_name", $username);
        $this->db->where("password", $password);
        $res = $this->db->get(config_item('rest_keys_table'));
        if ($res->num_rows() == 0) {
            return 0;
        } else {
            return 1;
        }
    }

       function get_package_usage($cus_id) {
        $s_date = $this->get_customer($cus_id, "start_date");
        if ($s_date == "0000-00-00") {
            return 0;
        } else {     


            $e_date = $this->cus_date($cus_id);

            // echo $e_date;

            $s_date = strtotime($s_date);

            $t_date = strtotime($this->todaydate());



           $interval=round(($t_date - $s_date) / (60*60*24), 0);
        

           return $interval;
          
            
        }
    }

     //prabash added


    function cus_date($cus_id) {
        $CI = & get_instance();

            $CI->db->where("cus_id", $cus_id);
            $res = $CI->db->get("customers");
            $timezone = "UTC";
            $timezone = ($timezone == "") ? "UTC" : $timezone;
            return $this->local_date($timezone);
    }
    
     function get_payment_method($pm_id, $field) {

        $this->db->select($field);

        $this->db->where("payment_method_id", $pm_id);

        $this->db->limit(1);

        $res = $this->db->get("payment_methods");

        return $res->row(0)->$field;
    }
    
      function mobile_credit_payment($phone_no, $amount, $acc_no, $trans_id, $c_code) {

        $query = "https://www.lycatv.tv/lverify/verify.php?phone_no=" . $phone_no . "&option=charge&amount=" . $amount . "&acc_no=" . $acc_no . "&trans_id=" . $trans_id . "&country_code=" . $c_code;

        //purchase from mobile credit
        try {
            $mc_deduct = json_decode(file_get_contents($query), true);
        } catch (Exception $e) {
            $mc_deduct["status"] = "error";
        }
        if ($mc_deduct["status"] == "low_balance") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "You don't have enough credit Balance.", "Please top up your balance and try again."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "failed") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.Please try again later."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "error") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.Please try again later."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "invalid") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.The mobile number is not a valid Lyca mobile number."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "blocked") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.Your Lyca mobile number has been blocked."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "expired") {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.Your Lyca mobile number has been inactive or expired."
            );
            return $resultArray;
        } else if ($mc_deduct["status"] == "success") {
            $resultArray = array(
                'err_type' => 1,
                'msg' => "Your Payment was success.Thank You for your payment.",
                'responce_arr' => $mc_deduct
            );
            return $resultArray;
        } else {
            $resultArray = array(
                'err_type' => 0,
                'msg' => "Your Payment was failed.Please try again later."
            );
            return $resultArray;
        }

        $resultArray = array(
            'err_type' => 0,
            'msg' => "Your Payment was failed.Nothing returned."
        );
        return $resultArray;
    }
    
    
    function get_customer_stb_allocated_ct($cus_id, $device_id) {
        $this->db->join("stb", "stb.cus_id = customers.cus_id");
        $this->db->where('customers.cus_id', $cus_id);
        $this->db->where('device_id', $device_id);
        $res = $this->db->get("customers");
        return $res->num_rows();
    }
    
       function add_xml($sno) {
        $x = "./authentication.xml";
        // Set the content type to be XML, so that the browser will   recognise it as XML.
        header("content-type: application/xml; charset=ISO-8859-15");
// "Create" the document.
        $xml = new DOMDocument("1.0", "ISO-8859-15");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;
        $xml->load($x);
        $x = $xml->documentElement;
// Create some elements.
        $xml_album = $xml->createElement("device", $sno);
        $x->appendChild($xml_album);
        $xml->save("./authentication.xml");
        //print $xml->saveXML();
    }

      function set_cus_status($cus_id) {
        $opt = $this->check_cus_stb($cus_id);
        $cus_status = $this->get_customer($cus_id, "customer_status");

        if ($cus_status == 8) {
                $cus_status = 9;
        }

        if ($cus_status == 1 || $cus_status == 3 || $cus_status == 7) {
            if ($opt == 0) {
                $cus_status = 1;
            } else {
                $cus_status = 3;
            }
        }
        $data = array(
            'customer_status' => $cus_status
        );
        $this->user_model->update_data("customers", $data, array('cus_id' => $cus_id));
    }

       function check_cus_stb($cus_id) {
//        $this->db->where("device_id", 1);
//        $this->db->or_where("device_id", 2);
//        $this->db->or_where("device_id", 11);
//        $this->db->or_where("device_id", 12);
//        $this->db->where("cus_id", $cus_id);
//        $qry = "SELECT * FROM (`customers_devices`) WHERE (`device_id` =  1 OR `device_id` =  2 OR `device_id` =  11 OR `device_id` =  19) AND `cus_id` =  $cus_id";

        $query = "SELECT customers_devices.device_id, customers_devices.no_of_login FROM customers_devices JOIN devices ON (devices.device_id = customers_devices.device_id) WHERE customers_devices.cus_id = $cus_id AND devices.stb_model = 1";
        $res = $this->db->query($query);
        if ($res->num_rows() == 0) {
            return 0;
        } else {
            $found = 0;
            foreach ($res->result_array() as $r) {
                $device_id = $r['device_id'];
                $no_of_login = $r['no_of_login'];
                $all_ct = $this->user_model->get_customer_stb_allocated_ct($cus_id, $device_id);
                if ($no_of_login > $all_ct) {
                    $found = 1;
                    break;
                }
            }
            return $found;
        }
    }

function _send_email($type, $email, $data, $subject, $resend = 0) {
//        $cus_id = 0;

        if (isset($data['refree_name'])) {
            $referee_name = $data['refree_name'];
        } else {
            $referee_name = "";
        }

        // check pin is available
        if (isset($data['pin_no'])) {
            $pin_no = $data['pin_no'];
        } else {
            $pin_no = "";
        }


        $wallet_balance = 0;
        $wallet_topup = 0;
        if (isset($data['cus_id']) && $data['cus_id'] != '') {


            // get wallet balance
            $wallet_balance = $this->user_model->get_field('cus_wallet', 'total_amount', array('cus_id' => $data['cus_id']));
            $wallet_topup = $this->user_model->get_field('cus_wallet_uploads', 'amount', array('cus_id' => $data['cus_id'], 'pymnt_r_upld' => 0, 'pc_id' => $data['payment_circle_id']));

            $f_name = $this->get_customer($data['cus_id'], "first_name");

            $l_name = $this->get_customer($data['cus_id'], "last_name");

            $title = $this->get_customer($data['cus_id'], "cus_title");

            $cus_id = $data['cus_id'];

            $cus_name = $title . '. ' . $f_name . ' ' . $l_name;

            if ($title == '') {
                $cus_name = $f_name . ' ' . $l_name;
            }


            if ($title != '') {
                $f_name = $f_name;
//                $f_name = $title . '. ' . $f_name;
            } else {
                $f_name = $f_name;
//                $f_name = 'Mr. ' . $f_name;
            }

            $user_name = $this->get_customer($data['cus_id'], "account_no");

            $password = $this->get_customer($data['cus_id'], "cus_password");

            $account_no = $this->get_customer($data['cus_id'], "account_no");

            $mobile = $this->get_customer($data['cus_id'], "mobile_no");

            $end_date = $this->get_customer($data['cus_id'], "end_date");
            $end_date = $this->user_model->uk_convert($end_date);
            $today = date("d-m-Y");

            $invoice_no = $this->get_customer($data['cus_id'], "account_no"); // to change
            // $charge_amt = $this->get_customer($data['cus_id'], "account_no"); // to change

            $package_description = $this->get_customer($data['cus_id'], "customer_package_description");

            $package = $this->get_data("customer_package_details", "*", array('cus_id' => $data['cus_id']));
            $cus_package_id = $package->row(0)->customer_package_id;
            $subscription_period_id = $package->row(0)->subscription_period_id;

            $subscription = $this->get_package_subscription2($cus_package_id, $subscription_period_id);

            $subscription_period = $subscription->row(0)->subscription_period_name;

            $subscription_charge_amt = $subscription->row(0)->package_amount;

            $card_no = $this->get_customer_payment_details($data['cus_id'], "card_no");
            if ($card_no != "") {
                $c = explode(' ', $card_no);
                $card_no = 'XXXX-XXXX-XXXX-' . $c[3];
            }
            /*             * ********** */
            if (isset($data['payment_circle_id']) && $data['payment_circle_id'] != '') {

                $data2['payment_circle_id'] = $data['payment_circle_id'];
                /* payment_type */
                $d_id = $this->user_model->get_payment_circle($data['payment_circle_id'], "d_id");
                $payment_detail_id = $this->user_model->get_payment_circle($data['payment_circle_id'], "payment_detail_id");
                $payment_type = "N/A";
                if ($d_id == 0 && $payment_detail_id == 0) {
                    
                } else {
                    if ($d_id == 0) {
                        $d_id = $this->user_model->get_payment_circle($data['payment_circle_id'], "payment_detail_id");
                        $direct_payments = $this->user_model->get_data('customers_payment_details', '*', array('payment_detail_id' => $d_id));
                        $payment_method_id = $direct_payments->row(0)->payment_method_id;
                    } else {
                        $direct_payments = $this->user_model->get_data('direct_payments', '*', array('d_id' => $d_id));
                        $payment_method_id = $direct_payments->row(0)->payment_method_id;
                    }
                    $pay = $this->user_model->get_data('payment_methods', 'payment_method', array('payment_method_id' => $payment_method_id));
                    $payment_type = $pay->row(0)->payment_method;
                }
                /* charge_amt */
                $charge_amt = $this->user_model->get_payment_circle($data['payment_circle_id'], "paid_amount");
                /* payment_insert_time */
                $payment_insert_time = $this->user_model->get_payment_circle($data['payment_circle_id'], "insert_time");
                $insert_time = explode(' ', $payment_insert_time);
                $payment_insert_time = $insert_time[0];
                $payment_insert_time = $this->user_model->uk_convert($payment_insert_time);
                /* $invoice_no  */
                $invoice_no = sprintf("%08d", $data['payment_circle_id']);
            } else {
                $payment_type = "";
                $charge_amt = "";
                $payment_insert_time = "";
                $invoice_no = "";
            }

            // making babies without taking effort (free device allocation)
            $dev_list = "";
            if (isset($data['device_list'])) {
                foreach (unserialize($data['device_list']) as $d) {
                    list($d_id, $d_count) = explode("-", $d);
                    $dev_nm = $this->get_field('devices', 'device_name', array('mg_id' => 2, 'device_id' => $d_id, 'status' => 1));
                    $dev_list .='<tr style="font-size: 13px;color: #333;font-family: HelveticaNeue,sans-serif;background-color:#F5F5F5;">
		<td style="height:35px;text-align:center;padding:10px 0 10px 0">' . $dev_nm . '</td>
		<td style="height:35px;text-align:center;padding:10px 0 10px 0">' . $d_count . '</td></tr>';
                }
            }


            /*             * ********** */
            $country_id = $this->get_customer($cus_id, "customers.country_id as con_id", "con_id");
            $currency_code = $this->get_country($country_id, "currency_code");

            // movie_details
            $mg_id = $this->get_customer($data['cus_id'], "customers.mg_id as mid", "mid");
//            $where = array('movie_packages.mg_id'=>$mg_id,'movie_packages.country_id'=>$country_id) ;
//            $mv_pack_id = $this->get_data_join('cus_movie_pack_subscription_details', 'movie_packages.movie_package_name,cus_movie_pack_subscription_details.subscription_end', 'movie_packages', 'cus_movie_pack_subscription_details.pack_id=movie_packages.movie_package_id', "inner", $where);
            $mv_pack_id = $this->get_field('cus_movie_pack_subscription_details', 'pack_id', array('cus_id' => $cus_id));
            $mv_pack_name = $this->get_field('movie_packages', 'movie_package_name', array('movie_package_id' => $mv_pack_id));
            $mv_sp_end = $this->get_field('cus_movie_pack_subscription_details', 'subscription_end', array('cus_id' => $cus_id));

            $shortcode = array(
                '[~f_name~]' => $f_name,
                //Yasitha Added these short code. Once Srimal come to the office please remove these and change the template
                '[~first_name~]' => $f_name,
                '[~cus_title~]' => $title,
                '[~account_no_insert~]' => $account_no,
                '[~account_no_insert_mobile~]' => $mobile,
                '[~pasword_generate~]' => $password,
                ////////////////////////////                
                '[~l_name~]' => $l_name,
                '[~last_name~]' => $l_name,
                '[~title~]' => $title,
                '[~cus_name~]' => $cus_name,
                '[~user_name~]' => $user_name,
                '[~password~]' => $password,
                '[~account_no~]' => $account_no,
                '[~mobile~]' => $mobile,
                '[~end_date~]' => $end_date,
                '[~today~]' => date('d-M-Y', strtotime($today)),
                '[~invoice_no~]' => $invoice_no,
                '[~card_no~]' => $card_no,
                '[~charge_amt~]' => $charge_amt,
                '[~subscription_charge_amt~]' => $subscription_charge_amt,
                '[~package_description~]' => $package_description,
                '[~subscription_period~]' => $subscription_period,
                '[~subscription_period_name~]' => $subscription_period,
                '[~payment_type~]' => $payment_type,
                '[~payment_insert_time~]' => $payment_insert_time,
                '[~currency_code~]' => $currency_code,
                '[~special_note~]' => 0,
                '[~movie_package_name~]' => $mv_pack_name,
                '[~mv_pack_exp_date~]' => date('d-M-Y', strtotime($mv_sp_end)),
                '[~wallet_balance~]' => $wallet_balance,
                '[~wallet_topup~]' => $wallet_topup,
                '[~free_device_list~]' => $dev_list,
                '[~referee_name~]' => $referee_name,
                '[~pin_no~]' => $pin_no
            );

            $data2['shortcode'] = $shortcode;
        }

        /* $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'mail.asiamediaglobal.org',
          'smtp_port' => 587,
          'smtp_user' => 'salitha@asiamediaglobal.org',
          'smtp_pass' => 'vxtv@123',
          'mailtype' => 'html'
          ); */

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => ' smtp.elasticemail.com',
            'smtp_port' => 2525,
            'smtp_user' => 'a7128719-f09d-4a01-95c6-5a297e3477f1',
            'smtp_pass' => 'a7128719-f09d-4a01-95c6-5a297e3477f1',
            'mailtype' => 'html'
        );
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // to get the email template according to the master agent we have to add 
        // array('mg_id'=> $mg_id, 'slug' => $type)
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        $content = $this->get_data("email_templates", "*", array('slug' => $type));

        foreach ($content->result() as $row) {

            $data2['content'] = $row->content;
        }

        //$m_name = $this->gm->get_data("email_templates", "title", array('slug' => $type));

        if (isset($data['attach']) && $data['attach'] != '') {
            $attach = $data['attach'];
        } else {
            $attach = "";
        }
        $subject = $this->get_data("email_templates", "title", array('slug' => $type));
        $subject = $subject->row(0)->title;
        $data2['m_name'] = $this->get_data("email_templates", "title", array('slug' => $type));
        $sdata = serialize($data2);
        $insert = array(
            'cus_id' => $cus_id,
            'subject' => $subject,
            'receiver_email' => $email,
            'data' => $sdata,
            'attach' => $attach,
            'email_template' => $type);

        if (isset($data['mg_id']) && $data['mg_id'] != '') {
            $mg_id = $data['mg_id'];
        } else {
            $mg_id = $this->user_model->get_customer($cus_id, "customers.mg_id as mid", "mid");
        }
     
        $email_sent = $this->get_master_agent($mg_id, "email_sent");
        if ($resend == 1) {
            $this->insert_data("email_queue", $insert);
        } else {
            if ($email_sent == 1) {
                $this->insert_data("email_queue", $insert);
            }
        }
        return TRUE;

//        $this->email->send();
//        echo $this->email->print_debugger();
    }
    
    function get_package_subscription2($cus_package_id, $subscription_period_id) {
        $this->db->select("*");
        $this->db->join("cus_packages_subscription_periods", "cus_packages_subscription_periods.subscription_period_id = subscription_periods.subscription_period_id");
        $this->db->where("cus_packages_subscription_periods.customer_package_id", $cus_package_id);
        $this->db->where("subscription_periods.subscription_period_id", $subscription_period_id);
        //$this->db->where("cus_packages_subscription_periods.show_on_web", 1);
        $this->db->order_by('no_of_days');
        return $this->db->get("subscription_periods");
    }

    function get_payment_circle($payment_circle_id, $field) {
        $this->db->select($field);
        $this->db->where("payment_circle_id", $payment_circle_id);
        $this->db->limit(1);
        $res = $this->db->get("payment_circle");
        return $res->row(0)->$field;
    }
    function get_customer_payment_details($cus_id, $field) {

        $this->db->select($field);

        $this->db->where("cus_id", $cus_id);

        $this->db->where("primary", 1);

        $this->db->limit(1);

        $res = $this->db->get("customers_payment_details");

        if ($res->num_rows() == 0) {

            return "";
        } else {

            return $res->row(0)->$field;
        }
    }
function uk_convert($date, $format = "%d-%m-%Y") {
    return mdate($format, strtotime($date));
}

      function payment_details($payment_id, $des, $amt, $unique_id = 0, $pay_type = 0, $qty = 1) {

        $data = array(
            'payment_circle_id' => $payment_id,
            'description' => $des,
            'amount' => $amt,
            'unique_id' => $unique_id,
            'qty' => $qty,
            'payment_type' => $pay_type
        );

        $this->db->insert("payment_circle_details", $data);
    }

      function get_customer_payment_details_other($cus_id) {

        $this->db->join("payment_methods", "payment_methods.payment_method_id = customers_payment_details.payment_method_id");


        $this->db->where("cus_id", $cus_id);

        $this->db->where("primary <>", 1);

        return $this->db->get("customers_payment_details");

    }

    //added for renewal
     function get_pack_no_dev_count($cus_package_id, $sp_id, $device_id) {
//        $this->db->select("no_of_items");
        $this->db->where("customer_package_id", $cus_package_id);
        $this->db->where("subscription_period_id", $sp_id);
        $this->db->where("device_id", $device_id);
        return $this->db->get("cus_packages_devices")->row(0)->no_of_items;
    }
    
        function get_channel_amt_renew_new($cus_id, $subscriptin_id, $packg_id) {
//        $this->db->join("channels", "countries_channels.channel_id = channels.id");
//        $this->db->join("channels", "countries_channels.channel_id = channels.id");
////        $this->db->where('cus_id', $cus_id);
//        $this->db->where('channel_amount <>', 0);
//        return $this->db->get("countries_channels");
//
//        $this->db->join("channels", "customers_channels.channel_id = channels.id");
//        $this->db->join("countries_channels", "countries_channels.channel_id = customers_channels.channel_id");
//        $this->db->where('countries_channels.subscription_period_id', $subscriptin_id);
//        $this->db->where('countries_channels.customer_package_id', $packg_id);
//        $this->db->where('customers_channels.cus_id', $cus_id);
//        $this->db->where('customers_channels.channel_amount <>', 0);
//        $this->db->where('customers_channels.priority', 999);
//        return $this->db->get("customers_channels");
        $query = "SELECT countries_channels.channel_amount AS channel_amount, channels.id, channels.display_name FROM (`customers_channels`) "
                . "JOIN `channels` ON `customers_channels`.`channel_id` = `channels`.`id` "
                . "JOIN `countries_channels` ON `countries_channels`.`channel_id` = `customers_channels`.`channel_id` "
                . "WHERE `countries_channels`.`subscription_period_id` = $subscriptin_id "
                . "AND `countries_channels`.`customer_package_id` = $packg_id "
                . "AND `customers_channels`.`cus_id` = $cus_id "
                . "AND `customers_channels`.`channel_amount` <> 0 "
                . "AND `customers_channels`.`priority` = 999";
        return $this->db->query($query);
    }
    
     function get_additional_channel_sub_renew($cus_id, $customer_package_id, $subscription_period_id) {

        $this->db->select('p.id,p.sub_package_name,p.amount,c.cus_amount');
        $this->db->from('customers_channels_sub_package c');
        $this->db->join('channels_sub_package p', 'c.sub_pack_id=p.id', 'inner');
        $this->db->where(array('c.cus_id' => $cus_id, 'p.pack_id' => $customer_package_id, 'p.subscription_id' => $subscription_period_id));
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function payment_log($payment_id, $payment_details_id, $status) {

//        $cus_id = $this->get_payment_circle($payment_id, "cus_id");

        $data = array(
            'payment_circle_id' => $payment_id,
            'payment_details_id' => $payment_details_id,
            'payment_status' => $status
        );

        $this->db->insert("customers_payment_log", $data);
    }
    
    function get_tmp_upgrade($cus_id) {
        $this->db->where('cus_id', $cus_id);
        return $this->db->get("tmp_customers_upgrade");
    }
    
    function get_device_amt_renew($cus_id) {
        $this->db->join("devices", "devices.device_id = customers_devices_amount.device_id");
        $this->db->where('cus_id', $cus_id);
        return $this->db->get("customers_devices_amount");
    }

    function get_channel_amt_renew($cus_id) {
        $this->db->join("channels", "channel_id = id");
        $this->db->where('cus_id', $cus_id);
        $this->db->where('channel_amount <>', 0);
        return $this->db->get("customers_channels");
    }
    
     
    function get_upgrade_package($cus_id, $field) {
        $this->db->select($field);
        $this->db->where("cus_id", $cus_id);
        $res = $this->db->get("tmp_customers_upgrade");
        return $res->row(0)->$field;
    }
    
     function delete_data($tbl_name, $field, $param) {
        $this->db->where($field, $param);
        $this->db->delete($tbl_name);
    }

       public function get_status_type($st) {
        if ($st == 0) {
            $str = '<span class="label label-danger show-clear">Unpaid</span>';
        } elseif ($st == 1) {
            $str = '<span class="label label-success show-clear">Activated</span>';
        } elseif ($st == 2) {
            $str = '<span class="label label-danger show-clear">Expired</span>';
        } elseif ($st == 3) {
            $str = '<span class="label label-primary show-clear">STB Pending</span>';
        } elseif ($st == 4) {
            $str = '<span class="label label-warning show-clear">Canceled</span>';
        } elseif ($st == 5) {
            $str = '<span class="label label-warning show-clear">Not Interested</span>';
        } elseif ($st == 6) {
            $str = '<span class="label label-danger show-clear">Suspended</span>';
        } elseif ($st == 69) {
            $str = '<span class="label label-danger show-clear">Deleted</span>';
        }

        return $str;
    }

    function user_activity_log($user_id, $activity) {
        date_default_timezone_set('Asia/Kolkata');
        $data = array(
            'user_id' => $user_id,
            'activity' => $activity,
            'last_updated_time' => date('Y-m-d H:i:s')
        );
        $this->db->insert('user_activities', $data);
    }

    function lyca_system_log($data) {
        $filename = 'system_log/system_log_' . date('Y-m-d') . '.txt';
        file_put_contents($filename, $data, FILE_APPEND);
    }
    
    function tentkotta_api_log($cus_id, $log_type, $log_date, $des = "", $unquie_id = 0) {

        $data = array(
            'cus_id' => $cus_id,
            'description' => $des,
            'log_date' => $log_date,
            'unique_id' => $unquie_id,
            'username' => "Cinematix",
            'last_update' => date('Y-m-d H:i:s'),
            'log_type' => $log_type
        );
        $this->db->insert("tentkotta_api_logs", $data);
    }

      function customers_log_return($cus_id, $log_type, $log_date, $des = "", $unquie_id = 0) {

        $data = array(
            'cus_id' => $cus_id,
            'description' => $des,
            'log_date' => $log_date,
            'unique_id' => $unquie_id,
            'username' => "API - Cinematix",
            'last_update' => date('Y-m-d H:i:s'),
            'log_type' => $log_type
        );

        if($this->db->insert("customers_log", $data)){
            return 1;
        }
        else {
           return 0;
        }
    }
    
    function update_cus_last_activated_history($cus_id, $action) {
        $user_id = 1000873;
        $action_date = date('Y-m-d H:i:s');

        $data = array(
            'last_activated_by' => $user_id
        );

        $param = array(
            'cus_id' => $cus_id
        );

        $this->update_data('customers', $data, $param);

        $data_insert = array(
            'cus_id' => $cus_id,
            'activator_id' => $user_id,
            'action' => $action,
            'update_date' => $action_date
        );

        $this->insert_data('customer_activator_history', $data_insert);
    }
    
}


?>